package math.linear.implementations;

import math.linear.interfaces.IMatrix;
import math.linear.models.AbstractMatrix;

/**
 * Class which is transpose view of another matrix. It hold reference on observed matrix.
 * @author ajuric
 *
 */
public class MatrixTransposeView extends AbstractMatrix {
	
	/**
	 * reference to original matrix
	 */
	private IMatrix original;

	/**
	 * Creates new instance of this class. Given matrix, <code>original</code>, is
	 * observed as transpose matrix.
	 * @param original matrix for transposition
	 */
	public MatrixTransposeView(IMatrix original) {
		super();
		this.original = original;
	}

	@Override
	public int getRowsCount() {
		return original.getColsCount();
	}

	@Override
	public int getColsCount() {
		return original.getRowsCount();
	}

	@Override
	public double get(int row, int col) {
		return original.get(col, row);
	}

	@Override
	public IMatrix set(int row, int col, double value) {
		original.set(col, row, value);
		return this;
	}

	@Override
	public IMatrix copy() {
		return new MatrixTransposeView(original.copy());
	}
	
	@Override
	public IMatrix newInstance(int rows, int cols) {
		return original.newInstance(rows, cols);
	}

}
