package math.linear.implementations;

import math.linear.exceptions.UnmodifiableObjectException;
import math.linear.interfaces.IVector;
import math.linear.models.AbstractVector;


/**
 * Implementation of {@link AbstractVector} class.
 * Implements methods specific for this vector implementation.
 * @author ajuric
 *
 */
public class Vector extends AbstractVector {
	
	/**
	 * for storing values
	 */
	private double[] elements;
	/**
	 * dimension of vector
	 */
	private int dimension;
	/**
	 * is it readOnly or not
	 */
	private boolean readOnly;

	/**
	 * Creates new modifiable instance of {@link Vector} with given <code>elements</code>
	 * which are copied into private array.
	 * @param elements
	 */
	public Vector(double... elements) {
		this(false, false, elements);
	}
	
	/**
	 * Creates new instance of {@link Vector} with given <code>elements</code>.
	 * <code>readOnly</code> tells if it is modifiable, <code>useGiven</code> tells
	 * if given <code>elements</code> should be copied into private array or should given
	 * reference be used
	 * @param readOnly indicates should new instance be read-only
	 * @param useGiven indicates should given reference be used or should copy elements into
	 * private array
	 * @param elements for new vector
	 */
	public Vector(boolean readOnly, boolean useGiven, double ... elements) {
		this.readOnly = readOnly;
		this.dimension = elements.length;
		if (useGiven) {
			this.elements = elements;
		} else {
			this.elements = new double[elements.length];
			for (int i = 0; i < elements.length; i++) {
				this.elements[i] = elements[i];
			}
		}
	}

	/**
	 * @throws IndexOutOfBoundsException if index is illegal
	 */
	@Override
	public double get(int index) {
		if (index < 0 || index >= dimension) {
			throw new IndexOutOfBoundsException("Allowed indices are from 0 to dimension - 1.");
		}
		
		return elements[index];
	}

	/**
	 * @throws IndexOutOfBoundsException if index is illegal
	 */
	@Override
	public IVector set(int index, double value)
			throws UnmodifiableObjectException {
		if (readOnly) {
			throw new UnmodifiableObjectException("Current IVector is readOnly");
		}
		if (index < 0 || index >= dimension) {
			throw new IndexOutOfBoundsException("Allowed indices are from 0 to dimension - 1.");
		}
		elements[index] = value;
		return this;
	}

	@Override
	public int getDimension() {
		return dimension;
	}

	@Override
	public IVector copy() {
		IVector copyOfCurrentVector = this.newInstance(this.dimension);
		for (int i = this.dimension - 1; i >= 0; i--) {
			copyOfCurrentVector.set(i, this.get(i));
		}
		return copyOfCurrentVector;
	}

	@Override
	public IVector newInstance(int dimension) {
		return new Vector(false, true, new double[dimension]);
	}
	
	/**
	 * Creates new instance of {@link IVector} from given string. Numbers in string
	 * should be separated with at least one space character. <br><br>
	 * Example:
	 * 
	 * <pre>Vector.parseSimple("1 2 3 4.5");</pre>
	 * 
	 * gives new vector with values 1 2 3 and 4.5.
	 * @param vector to be parsed
	 * @return new vector from string
	 * @throws IllegalArgumentException if given string is empty or values are not
	 * double values
	 */
	public static IVector parseSimple(String vector) {
		if (vector.length() == 0) {
			throw new IllegalArgumentException("String cannot be empty.");
		}
		String[] arguments = vector.split("[\\s]+");
		double[] elements = new double[arguments.length];
		for (int i = arguments.length - 1; i >= 0; i--) {
			try {
				elements[i] = Double.parseDouble(arguments[i]);
			} catch (Exception e) {
				throw new IllegalArgumentException("Given values must be double, '" 
						+ arguments[i] + "' received.");
			}
		}
		
		return new Vector(false, true, elements);
	}

}
