package math.linear.implementations;

import math.linear.interfaces.IMatrix;
import math.linear.models.AbstractMatrix;

/**
 * View on another matrix.
 * @author ajuric
 */

public class MatrixSubMatrixView extends AbstractMatrix {
	
	/**
	 * Reference to original matrix.
	 */
	private IMatrix original;
	/**
	 * Indices of rows used.
	 */
	private int[] rowIndexes;
	/**
	 * Indices of columns used.
	 */
	private int[] colIndexes;
	
	/**
	 * Creates new instance of this class with given parameters.
	 * @param original of observed matrix
	 * @param row which is being ignored
	 * @param col which is being ignored
	 */
	public MatrixSubMatrixView(IMatrix original, int row, int col) {
		this.original = original;
		checkIndices(row, col);
		rowIndexes = fillWithout(this.original.getRowsCount(), row);
		colIndexes = fillWithout(this.original.getColsCount(), col);
	}
	
	/**
	 * Creates new array of integer which size if <code>size-1</code> with numbers
	 * from 0 to size-1 but without <code>index</code>.
	 * @param size of new array
	 * @param index of number not included
	 * @return new array which size is <code>size-1</code>
	 */
	private int[] fillWithout(int size, int index) {
		int[] list = new int[size-1];
		int passedThrownOut = 0;
		for (int i = 0; i < size; i++) {
			if (i == index) {
				passedThrownOut = 1;
				continue;
			}
			list[i - passedThrownOut] = i; 
		}
			
		return list;
	}
	
	/**
	 * Private constructor.
	 * @param original reference on original matrix
	 * @param rowIndexes index of rows supported
	 * @param colIndexes index of columns supported
	 */
	private MatrixSubMatrixView(IMatrix original, int[] rowIndexes, int[] colIndexes) {
		this.original = original;
		this.rowIndexes = rowIndexes;
		this.colIndexes = colIndexes;
	}

	@Override
	public int getRowsCount() {
		return rowIndexes.length;
	}

	@Override
	public int getColsCount() {
		return colIndexes.length;
	}

	@Override
	public double get(int row, int col) {
		checkIndices(row, col);
		return original.get(rowIndexes[row], colIndexes[col]);
	}

	@Override
	public IMatrix set(int row, int col, double value) {
		checkIndices(row, col);
		original.set(rowIndexes[row], colIndexes[col], value);
		return this;
	}
	
	/**
	 * Checks if given indices are legal.
	 * @param row to be checked
	 * @param col to be checked
	 * @throw {@link IndexOutOfBoundsException} if given indices are illegal
	 */
	private void checkIndices(int row, int col) {
		if (row < 0 || row >= this.original.getRowsCount()) {
			throw new IndexOutOfBoundsException("Legal indices for row are [0, rows - 1].");
		}
		if (col < 0 || col >= this.original.getColsCount()){
			throw new IndexOutOfBoundsException("Legal indices for col are [0, cols - 1].");
		}
	}

	@Override
	public IMatrix copy() {
		IMatrix newMatrix = original.newInstance(this.getRowsCount(), this.getColsCount());
		for (int i = 0, rows = this.getRowsCount(), cols = this.getColsCount(); i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				newMatrix.set(i, j, this.get(i, j));
			}
		}
		
		return newMatrix;
	}

	@Override
	public IMatrix newInstance(int rows, int cols) {
		return original.newInstance(rows, cols);
	}
	
	@Override
	public IMatrix subMatrix(int row, int col, boolean liveView) {
		return new MatrixSubMatrixView(
			liveView ? original : original.copy(),
			fillWithout(this.getRowsCount(), row),
			fillWithout(this.getColsCount(), col)
		);
	}

}
