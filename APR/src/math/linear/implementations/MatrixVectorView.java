package math.linear.implementations;

import math.linear.defaults.LinAlgDefaults;
import math.linear.interfaces.IMatrix;
import math.linear.interfaces.IVector;
import math.linear.models.AbstractMatrix;

/**
 * View on some vector as it is one-row or one-column matrix.
 * @author ajuric
 *
 */
public class MatrixVectorView extends AbstractMatrix {
	
	/**
	 * reference on original vector
	 */
	private IVector original;
	/**
	 * indicates whether this is one-row or one-column matrix
	 */
	private boolean asRowMatrix;

	/**
	 * Creates new instance of this class.
	 * @param original reference on original matrix
	 * @param asRowMatrix if <code>true</code>, this is one-row matrix; otherwise it
	 * is one-column matrix
	 */
	public MatrixVectorView(IVector original, boolean asRowMatrix) {
		super();
		this.original = original;
		this.asRowMatrix = asRowMatrix;
	}

	@Override
	public int getRowsCount() {
		return asRowMatrix ? 1 : original.getDimension();
	}

	@Override
	public int getColsCount() {
		return asRowMatrix ? original.getDimension() : 1;
	}

	@Override
	public double get(int row, int col) {
		checkIndex(row, col);
		return original.get(asRowMatrix ? col : row);
		
	}

	@Override
	public IMatrix set(int row, int col, double value) {
		checkIndex(row, col);
		original.set(asRowMatrix ? col : row, value);
		return this;
	}
	
	/**
	 * Checks if given indices are legal.
	 * @param row to be checked
	 * @param col to be checked
	 * @throws IndexOutOfBoundsException if given indices are illegal
	 */
	private void checkIndex(int row, int col) {
		if (asRowMatrix && row > 0) {
			throw new IndexOutOfBoundsException("Matrix has exactly one row.");
		}
		if (!asRowMatrix && col > 0) {
			throw new IndexOutOfBoundsException("Matrix has exactly one column.");
		}
	}

	@Override
	public IMatrix copy() {
		return new MatrixVectorView(original.copy(), asRowMatrix);
	}

	@Override
	public IMatrix newInstance(int rows, int cols) {
		return LinAlgDefaults.defaultMatrix(rows, cols);
	}

}
