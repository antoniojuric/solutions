package math.linear.implementations;

import math.linear.defaults.LinAlgDefaults;
import math.linear.exceptions.UnmodifiableObjectException;
import math.linear.interfaces.IMatrix;
import math.linear.interfaces.IVector;
import math.linear.models.AbstractVector;

/**
 * View on matrix as given matrix is vector.
 * @author ajuric
 *
 */
public class VectorMatrixView extends AbstractVector {
	
	/**
	 * Reference on original matrix.
	 */
	private IMatrix original;
	/**
	 * Dimension of vector. 
	 */
	private int dimension;
	/**
	 * Indicates if given matrix is one-row or one-column matrix.
	 */
	private boolean rowMatrix;
	
	/**
	 * Creates new instance of this class from given <code>original</code>.
	 * @param original of matrix
	 * @throws IllegalArgumentException if given <code>original</code> matrix is neither
	 * one-row nor one-column matrix
	 */
	public VectorMatrixView(IMatrix original) {
		if (original.getRowsCount() == 1) {
			this.rowMatrix = true;
		} else if (original.getColsCount() == 1) {
			this.rowMatrix = false;
		} else {
			throw new IllegalArgumentException("Given matrix must have only one row or only one column.");
		}
		this.dimension = rowMatrix ? original.getColsCount() : original.getRowsCount();
		this.original = original;
	}

	@Override
	public double get(int index) {
		return original.get(rowMatrix ? 0 : index, rowMatrix ? index : 0);
	}

	@Override
	public IVector set(int index, double value)
			throws UnmodifiableObjectException {
		original.set(rowMatrix ? 0 : index, rowMatrix ? index : 0, value);
		return this;
	}

	@Override
	public int getDimension() {
		return dimension;
	}

	@Override
	public IVector copy() {
		return new VectorMatrixView(original.copy()); 
	}

	@Override
	public IVector newInstance(int dimension) {
		return LinAlgDefaults.defaultVector(dimension);
	}

}
