package math.linear.implementations;

import java.util.Arrays;

import math.linear.exceptions.IncompatibleOperandException;
import math.linear.interfaces.IMatrix;
import math.linear.models.AbstractMatrix;


/**
 * Implementation of {@link AbstractMatrix} class.
 * Implements methods specific for this matrix implementation.
 * @author ajuric
 *
 */
public class Matrix extends AbstractMatrix {

	/**
	 * for storing values
	 */
	private double[][] elements;
	/**
	 * number of rows
	 */
	private int rows;
	/**
	 * number of columns
	 */
	private int cols;
	
	/**
	 * Creates new instance of {@link Matrix} with given size filled with zeros.
	 * @param rows
	 * @param cols
	 */
	public Matrix(int rows, int cols) {
		this(rows, cols, new double[rows][cols], true);
	}
	
	/**
	 * Creates new instance of {@link Matrix} with given parameters. <br>
	 * If <code>useGiven</code> is <code>true</code>, then they are used for local
	 * values; they are copied otherwise.
	 * @param rows number of rows
	 * @param cols number of columns
	 * @param elements for matrix
	 * @param useGiven indicator whether or not to use given <code>elements</code> or to copy them 
	 */
	public Matrix(int rows, int cols, double[][] elements, boolean useGiven) {
		if (rows != elements.length) {
			throw new IllegalArgumentException("Parameter 'rows' and number of rows in elements must be equal.");
		}
		if (rows == 0) {
			throw new IllegalArgumentException("There must be at least one row.");
		}
		if (cols != elements[0].length) {
			throw new IllegalArgumentException("Parameter 'cols' and number of columns in elements must be equal.");
		}		
		
		this.rows = rows;
		this.cols = cols;
		if (useGiven) {
			this.elements = elements;
		} else {
			this.elements = new double[rows][cols];
			for (int i = 0; i < rows; i++) {
				for (int j = 0; j < cols; j++) {
					this.elements[i][j] = elements[i][j];
				}
			}
		}
	}

	@Override
	public int getRowsCount() {
		return rows;
	}

	@Override
	public int getColsCount() {
		return cols;
	}

	/**
	 * @throws IndexOutOfBoundsException if indices are not within legal bounds
	 */
	@Override
	public double get(int row, int col) {
		checkIndices(row, col);
		return elements[row][col];
	}

	/**
	 * @throws IndexOutOfBoundsException if indices are not within legal bounds
	 */
	@Override
	public IMatrix set(int row, int col, double value) {
		checkIndices(row, col);
		elements[row][col] = value;
		return this;
	}
	
	/**
	 * Checks given indices if they are within legal indices. Legal indices are from
	 * 0 to rows - 1 for <code>row</code>, and from 0 to cols - 1 for <code>col</code>.
	 * @param row to be checked
	 * @param col to be checked
	 * @throws IncompatibleOperandException if indices are illegal
	 */
	private void checkIndices(int row, int col) {
		if (row < 0 || row >= rows) {
			throw new IndexOutOfBoundsException("Legal indices for row are: 0 to rows - 1.");
		}
		if (col < 0 || col >= cols){
			throw new IndexOutOfBoundsException("Legal indices for col are: 0 to cols - 1.");
		}
	}

	@Override
	public IMatrix copy() {
		IMatrix newMatrix = this.newInstance(rows, cols);
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				newMatrix.set(i, j, this.get(i, j));	
			}
		}
		
		return newMatrix;
	}

	@Override
	public IMatrix newInstance(int rows, int cols) {
		return new Matrix(rows, cols);
	}
	
	/**
	 * Creates new instance of {@link Matrix} from given string. Every row of 
	 * matrix string representation must have equal number of elements. Rows are
	 * separated by single '|' character and there must be at least one space character
	 * between numbers.<br><br>
	 * 
	 * Example: <pre>Matrix.parseSimple("1 2 3 | -2 3 4")</pre>
	 * creates new instance of matrix with numbers 1, 2 and 3 in first row and 
	 * -2, 3 and 4 in second row.
	 * @param matrix to be created
	 * @return new matrix
	 */
	public static Matrix parseSimple(String matrix) {
		if (matrix.length() == 0) {
			throw new IllegalArgumentException("Empty string cannot be parsed.");
		}
		String[] rows = matrix.split("[\\s]*[|][\\s]*");
		int rowSize = rows.length;
		int colSize = rows[0].trim().split("[\\s]+").length;
		double[][] elements = new double[rowSize][colSize];
		for (int i = 0; i < rowSize; i++) {
			String[] cols = rows[i].trim().split("[\\s]+");
			if (cols.length != colSize) {
				throw new IllegalArgumentException("Every row must be of equal size.");
			}
			for (int j = 0; j < colSize; j++) {
				try {
					elements[i][j] = Double.parseDouble(cols[j]);
				} catch (Exception e) {
					throw new IllegalArgumentException("Given values must be double, '" 
							+ cols[i] + "' received.");
				}
			}
		}
		
		return new Matrix(rowSize, colSize, elements, true);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + cols;
		result = prime * result + Arrays.deepHashCode(elements);
		result = prime * result + rows;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (getClass() != obj.getClass())
			return false;
		Matrix other = (Matrix) obj;
		if (cols != other.cols)
			return false;
		if (!Arrays.deepEquals(elements, other.elements))
			return false;
		if (rows != other.rows)
			return false;
		return true;
	}
	
	

}
