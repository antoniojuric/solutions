package math.linear.demonstration;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import math.linear.implementations.Matrix;
import math.linear.interfaces.IMatrix;

/**
 * Demonstration 3. Calculates baricentric coordinates of point on a plane given with points of triangle.
 * @author ajuric
 *
 */
public class Demonstration3 {
	
	/**
	 * Entry point. 
	 * @param args not used here
	 * @throws IOException errors while reading from standard input
	 */
	public static void main(String[] args) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Unesite koordinate točke A trokuta:");
		String[] Ac = reader.readLine().split("[\\s]");
		System.out.println("Unesite koordinate točke B trokuta:");
		String[] Bc = reader.readLine().split("[\\s]");
		System.out.println("Unesite koordinate točke C trokuta:");
		String[] Cc = reader.readLine().split("[\\s]");
		
		System.out.println("Unesite koordinate točke T koja leži u ravnini trokuta:");
		String[] Tc = reader.readLine().split("[\\s]");
		
		IMatrix A = Matrix.parseSimple(Ac[0] + " " + Bc[0] + " " + Cc[0] + " | " 
								     + Ac[1] + " " + Bc[1] + " " + Cc[1] + "| 1 1 1");
		IMatrix T = Matrix.parseSimple(Tc[0] + " | " + Tc[1] + " | 1");
		
		IMatrix t = A.nInvert().nMultiply(T);
		System.out.println("t = " + t.toVector(true));
		
	}

}
