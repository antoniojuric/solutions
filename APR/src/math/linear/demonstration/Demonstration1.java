package math.linear.demonstration;

import math.linear.implementations.Matrix;
import math.linear.implementations.Vector;
import math.linear.interfaces.IMatrix;
import math.linear.interfaces.IVector;

/**
 * Demonstration 1. Prints solutions of vectors and matrices.
 * @author ajuric
 *
 */
public class Demonstration1 {
	
	/**
	 * Entry point.
	 * @param args not used here
	 */
	public static void main(String[] args) {
		IVector v1 = Vector.parseSimple("2 3 -4").add(Vector.parseSimple("-1 4 -3"));
		System.out.println("v1 = " + v1);
		
		double s = v1.scalarProduct(Vector.parseSimple("-1 4 -3"));
		System.out.println("s = " + s);
		
		IVector v2 = v1.nVectorProduct(Vector.parseSimple("2 2 4"));
		System.out.println("v2 = " + v2);
		
		IVector v3 = v2.nNormalize();
		System.out.println("v3 = " + v3);
		
		IVector v4 = v2.scalarMultiply(-1);
		System.out.println("v4 = " + v4);
		
		IMatrix M1 = Matrix.parseSimple("1 2 3 | 2 1 3 | 4 5 1").add(
					 Matrix.parseSimple("-1 2 -3 | 5 -2 7 | -4 -1 3"));
		System.out.println("M1 = ");
		System.out.println(M1);
		
		IMatrix M2 = Matrix.parseSimple("1 2 3 | 2 1 3 | 4 5 1").nMultiply(
					 Matrix.parseSimple("-1 2 -3 | 5 -2 7 | -4 -1 3").nTranspose(true));
		System.out.println("M2 = ");
		System.out.println(M2);
		
		IMatrix M3 = Matrix.parseSimple("-24 18 5 | 20 -15 -4 | -5 4 1").nInvert().nMultiply(
				Matrix.parseSimple("1 2 3 | 0 1 4 | 5 6 0").nInvert());
		System.out.println("M3 = ");
		System.out.println(M3);
	}

}
