package math.linear.demonstration;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import math.linear.implementations.Matrix;
import math.linear.interfaces.IMatrix;

/**
 * Demonstration 2. Expects numbers for 3*3 array of coefficients and resulting vector.
 * Numbers should be entered in 3 rows likewise: <br>
 * <code>a11 a12 a13 r1 <br>
 * a21 a22 a23 r2 <br>
 * a31 a32 a33 r3</code> 
 * @author ajuric
 *
 */
public class Demonstration2 {
	
	/**
	 * Entry point.
	 * @param args not used here
	 * @throws IOException errors while reading from standard input
	 */
	public static void main(String[] args) throws IOException {
		double[][] a = new double[3][3];
		double[] r = new double[3];
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		for (int i = 0; i < 3; i++) {
			String[] coefficients = reader.readLine().split("[\\s]");
			for (int j = 0; j < 3; j++) {
				a[i][j] = Double.parseDouble(coefficients[j]);
			}
			r[i] = Double.parseDouble(coefficients[3]);
		}
		
		IMatrix A = Matrix.parseSimple(a[0][0] + " " + a[0][1] + " " + a[0][2] + " | " 
								     + a[1][0] + " " + a[1][1] + " " + a[1][2] + " | "
								     + a[2][0] + " " + a[2][1] + " " + a[2][2]);
		IMatrix R = Matrix.parseSimple(r[0] + " | " + r[1] + " | " + r[2]);
		
		IMatrix v = A.nInvert().nMultiply(R);
		System.out.println("[x, y, z] = " + v.toVector(true));
	}

}
