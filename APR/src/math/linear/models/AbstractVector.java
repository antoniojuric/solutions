package math.linear.models;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import math.linear.exceptions.IncompatibleOperandException;
import math.linear.exceptions.UnmodifiableObjectException;
import math.linear.implementations.MatrixVectorView;
import math.linear.interfaces.IMatrix;
import math.linear.interfaces.IVector;

/**
 * Implementation of {@link IVector} interface.
 * Implements methods which are independent from vector implementation.
 * @author ajuric
 *
 */
public abstract class AbstractVector implements IVector {

	/**
	 * for comparing real numbers
	 */
	private static final double DELTA = 1e-7;

	public abstract double get(int index);

	public abstract IVector set(int index, double value)
			throws UnmodifiableObjectException;

	public abstract int getDimension();

	public abstract IVector copy();
	
	public abstract IVector newInstance(int dimension);

	/**
	 * @throws IllegalArgumentException if provided <code>n</code> is not positive integer
	 */
	public IVector copyPart(int n) {
		if (n <= 0) {
			throw new IllegalArgumentException("Provided n must be positive integer.");
		}
		IVector newVector = newInstance(n);
		for (int i = 0; i < n; i++) {
			newVector.set(i, 0);
		}
		int length = Integer.min(this.getDimension(), n);
		for (int i = 0; i < length; i++) {
			newVector.set(i, this.get(i));
		}		
		return newVector;
	}

	public IVector add(IVector other) throws IncompatibleOperandException {
		if (this.getDimension() != other.getDimension()) {
			throw new IncompatibleOperandException("Vectors are not of same dimensions.");
		}		
		for (int i = this.getDimension() - 1; i >= 0; i--) {
			this.set(i, this.get(i) + other.get(i));
		}		
		return this;
	}

	public IVector nAdd(IVector other) throws IncompatibleOperandException {
		return this.copy().add(other);
	}

	public IVector sub(IVector other) throws IncompatibleOperandException {
		if (this.getDimension() != other.getDimension()){
			throw new IncompatibleOperandException("Vectors are not of same dimensions.");
		}		
		for (int i = this.getDimension() - 1; i >= 0; i--) {
			this.set(i, this.get(i) - other.get(i));
		}		
		return this;
	}

	public IVector nSub(IVector other) throws IncompatibleOperandException {
		return this.copy().sub(other);
	}

	public IVector scalarMultiply(double byValue) {
		for (int i = this.getDimension() - 1; i >= 0; i--) {
			this.set(i, this.get(i) * byValue);
		}
		return this;
	}

	public IVector nScalarMultiply(double byValue) {
		return this.copy().scalarMultiply(byValue);
	}

	public double norm() {
		double norm = 0;
		for (int i = this.getDimension() - 1; i >= 0; i--) {
			norm += this.get(i) * this.get(i);
		}
		return Math.sqrt(norm);
	}

	/**
	 * @throws IncompatibleOperandException if norm of current vector is 0
	 */
	public IVector normalize() {
		double norm = this.norm();
		if (Math.abs(norm) < DELTA){
			throw new IncompatibleOperandException("Length of vector is zero.");
		}
		
		for (int i = this.getDimension() - 1; i >= 0; i--) {
			this.set(i, this.get(i) / norm);
		}
		
		return this;
	}

	public IVector nNormalize() {
		return this.copy().normalize();
	}

	public double cosine(IVector other) throws IncompatibleOperandException {
		if (this.getDimension() != other.getDimension()) {
			throw new IncompatibleOperandException("Vectors are not of same dimensions.");
		}		
		return this.scalarProduct(other) / (this.norm() * other.norm());
	}

	public double scalarProduct(IVector other)
			throws IncompatibleOperandException {
		if (this.getDimension() != other.getDimension()) {
			throw new IncompatibleOperandException("Vectors are not of same dimensions.");
		}
		double scalarProduct = 0;
		for (int i = this.getDimension() - 1; i >= 0; i--) {
			scalarProduct += this.get(i) * other.get(i);
		}
		return scalarProduct;
	}

	/**
	 * Used implementation: <a href="http://en.wikipedia.org/wiki/Cross_product#Matrix_notation">
	 * Matrix notation of cross-product</a>.
	 */
	public IVector nVectorProduct(IVector other)
			throws IncompatibleOperandException {
		if (this.getDimension() != 3 || other.getDimension() != 3) {
			throw new IncompatibleOperandException("Vector must have size 3.");
		}
		
		double[] elements = new double[3];
		elements[0] = this.get(1)  * other.get(2) - other.get(1) * this.get(2);
		elements[1] = other.get(0) * this.get(2)  - this.get(0)  * other.get(2);
		elements[2] = this.get(0)  * other.get(1) - other.get(0) * this.get(1);
		
		IVector crossProduct = this.newInstance(3); // because implementation of object which
													// called this method will decide about
													// implementation of new vector
		for (int i = 0; i < elements.length; i++) {
			crossProduct.set(i, elements[i]);
		}
		return crossProduct;
	}

	/**
	 * @throws IncompatibleOperandException if current vector is not in homogeneous space
	 * or last coordinate is 0
	 */
	public IVector nFromHomogeneus() {
		if (this.getDimension() == 1) {
			throw new IncompatibleOperandException("Vector of dimension 1 is not in homogeneous space.");
		}
		IVector newVector = this.copyPart(this.getDimension() - 1);
		double lastCoordinate = this.get(this.getDimension() - 1);
		if (Math.abs(lastCoordinate) < DELTA) {
			throw new IncompatibleOperandException("Last coordinate of current vector is zero.");
		}		
		for (int i = this.getDimension() - 2; i >= 0; i--) {
			newVector.set(i, this.get(i) / lastCoordinate);
		}
		
		return newVector;
	}

	public IMatrix toRowMatrix(boolean liveView) {
		return new MatrixVectorView(liveView ? this : this.copy(), true);
	}

	public IMatrix toColumnMatrix(boolean liveView) {
		return new MatrixVectorView(liveView ? this : this.copy(), false);
	}

	public double[] toArray() {
		double[] vector = new double[this.getDimension()];
		for (int i = this.getDimension() - 1; i >= 0; i--) {
			vector[i] = this.get(i);
		}
		
		return vector;
	}

	/**
	 * Formats current vector in a way that every number is printed with given precision.
	 * @param precision of vector values
	 * @return a string representation of object
	 */
	public String toString(int precision) {
		/*StringBuilder stringBuilder = new StringBuilder(
			(precision + LEADING_DIGITS) * this.getDimension());*/
		PrintStream oldStream = System.out;
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		PrintStream printStream = new PrintStream(baos);
		System.setOut(printStream);
		//stringBuilder.append(System.out.format("["));
		System.out.format("[");
		for (int i = 0, dimension = this.getDimension(); i < dimension; i++) {
			//stringBuilder.append(System.out.format("%."+precision+"f%n", this.get(i)));
			System.out.format("%."+precision+"f", this.get(i));			
			if (i < dimension - 1) {
				//stringBuilder.append(System.out.format(", "));
				System.out.format(", ");
			}
		}
		//stringBuilder.append(System.out.format("]"));
		System.out.format("]");
		
		System.out.flush();
		System.setOut(oldStream);
		
		return baos.toString();
	}
	
	@Override
	public String toString() {
		return this.toString(3);
	}

}
