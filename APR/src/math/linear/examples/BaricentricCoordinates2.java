package math.linear.examples;

import math.linear.implementations.Matrix;
import math.linear.interfaces.IMatrix;

/**
 * Example with calculations for baricentric coordinates on other way.
 * @author ajuric
 *
 */
public class BaricentricCoordinates2 {
	
	/**
	 * Entry point.
	 * @param args not used here
	 */
	public static void main(String[] args) {
		IMatrix A = Matrix.parseSimple("1 5 3 | 0 0 8 | 1 1 1");
		IMatrix T = Matrix.parseSimple("3 | 4 | 1");
		IMatrix t = A.nInvert().nMultiply(T);
		System.out.println(t);
	}

}
