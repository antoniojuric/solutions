package math.linear.examples;

import math.linear.implementations.Matrix;
import math.linear.interfaces.IMatrix;

/**
 * Example with solving equation system.
 * @author ajuric
 *
 */
public class EquationSystem {
	
	/**
	 * Entry point.
	 * @param args not used
	 */
	public static void main(String[] args) {
		IMatrix a = Matrix.parseSimple("3 5 | 2 10");
		IMatrix r = Matrix.parseSimple("2 | 8");
		IMatrix v = a.nInvert().nMultiply(r);
		System.out.println("Rjesenje sustava je:");
		System.out.println(v);
	}

}
