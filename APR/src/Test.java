import java.awt.Image;

import math.linear.defaults.LinAlgDefaults;
import math.linear.interfaces.IMatrix;

public class Test {
	
	public static void main(String[] args) {
		IMatrix m = LinAlgDefaults.readFromFileDefaultMatrix("matrix.txt");
		System.out.println(m);
		//LinAlgDefaults.writeToFile(m, "matrix-new.txt");
		
		IMatrix m2 = m.copy();
		System.out.println(m.equals(m2));
		
		m2.scalarMultiply(2+5);
		m2.scalarMultiply(1/7.);
		
		System.out.println(m.equals(m2));
		
		System.out.println(m2.decompositionLU());
		System.out.println(m2);
		System.out.println(m2.decompositionLU());
	}

}
