package hr.fer.zemris.apr.lab1;

import math.linear.defaults.LinAlgDefaults;
import math.linear.interfaces.IMatrix;
import math.linear.wrappers.LUPWrapper;

public class Task3 {
	
	public static void main(String[] args) {
		
		/*IMatrix A = LinAlgDefaults.readFromFileDefaultMatrix("zad3-A.mat");
		IMatrix b = LinAlgDefaults.readFromFileDefaultMatrix("zad3-b.mat");
		
		System.out.println("A:\n" + A);
		System.out.println("b:\n" + b);

		IMatrix LU = A.nDecompositionLU();
		
		System.out.println("LU:\n" + LU);
		
		IMatrix y = LU.nSubstitutionForward(b);
		IMatrix x = LU.nSubstitutionBackward(y);
		
		System.out.println("x:\n" + x);
		
		System.out.println();
		System.out.println();*/
		
		// these are three equal equations!!!
		
		IMatrix A2 = LinAlgDefaults.readFromFileDefaultMatrix("zad3-A.mat");
		IMatrix b2 = LinAlgDefaults.readFromFileDefaultMatrix("zad3-b.mat");
		
		System.out.println(A2.determinant());
		
		System.out.println("A2:\n" + A2);
		System.out.println("b2:\n" + b2);
		
		LUPWrapper wrapper = A2.decompositionLUP();
		IMatrix LU2 = wrapper.getLUPMatirx().nPermuteRows(wrapper.getP());
		b2 = b2.nPermuteRows(wrapper.getP());
		
		System.out.println("LU2:\n" + LU2);
		System.out.println("P*b2:\n" + b2);
		
		IMatrix y2 = LU2.nSubstitutionForward(b2);
		IMatrix x2 = LU2.nSubstitutionBackward(y2); 
		
		System.out.println("y2:\n" + y2);
		System.out.println("x2:\n" + x2);
		
	}

}
