package hr.fer.zemris.apr.lab1;

import math.linear.defaults.LinAlgDefaults;
import math.linear.interfaces.IMatrix;
import math.linear.wrappers.LUPWrapper;

public class Task5 {
	
	public static void main(String[] args) {
		/*IMatrix A1 = LinAlgDefaults.readFromFileDefaultMatrix("zad5-A.mat");
		IMatrix b1 = LinAlgDefaults.readFromFileDefaultMatrix("zad5-b.mat");
		
		System.out.println("A1:\n" + A1);
		System.out.println("b1:\n" + b1);
		
		// will crash because in algorithm there is zero on diagonal
		IMatrix LU1 = A1.nDecompositionLU();
		
		System.out.println("LU1:\n" + LU1);
		
		IMatrix y1 = LU1.nSubstitutionForward(b1);
		IMatrix x1 = LU1.nSubstitutionBackward(y1);
		
		System.out.println("x1:\n" + x1);
		
		System.out.println();
		System.out.println();*/
		
		IMatrix A2 = LinAlgDefaults.readFromFileDefaultMatrix("zad5-A.mat");
		IMatrix b2 = LinAlgDefaults.readFromFileDefaultMatrix("zad5-b.mat");
		
		System.out.println("A2:\n" + A2);
		System.out.println("b2:\n" + b2);
		
		LUPWrapper wrapper = A2.decompositionLUP();
		IMatrix LU2 = wrapper.getLUPMatirx().nPermuteRows(wrapper.getP());
		b2 = b2.nPermuteRows(wrapper.getP());
		
		System.out.println("LU2:\n" + LU2);
		System.out.println("P*b2:\n" + b2);
		
		IMatrix y2 = LU2.nSubstitutionForward(b2);
		IMatrix x2 = LU2.nSubstitutionBackward(y2); 
		
		System.out.println("y2:\n" + y2);
		System.out.println("x2:\n" + x2);
	}

}
