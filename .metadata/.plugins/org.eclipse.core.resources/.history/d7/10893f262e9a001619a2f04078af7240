package hr.fer.zemris.java.dz3;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class RegresijaSustava {
	
	public static void main(String[] args) {
		if (args.length != 2) {
			System.out.println("Expected: filename method");
			return;
		}

		String fileName = args[0];
		String method = args[1];
		
		ReadingWrapper readingWrapper = readFromFile(fileName);
		IFunction transferFunction = new TransferFunction(readingWrapper.getX(), readingWrapper.getY());
		
		if (method.startsWith("decimal")) {
			IOptAlgorithm<DoubleArraySolution> algorithm = new SimulatedAnnealing<>(
					new PassThroughDecoder(),
					new DoubleArrayUnifNeighborhood(new double[]{0.5, 0.5, 0.5, 0.5, 0.5, 0.5}),
					new DoubleArraySolution(6),
					transferFunction,
					new GeometricTempSchedule(0.95, 1000000, 1000, 1000),
					true);
			algorithm.run();
		} else if (method.startsWith("binary:")) {
			int bits = Integer.parseInt(method.substring(7));
			IOptAlgorithm<BitVectorSolution> algorithm = new SimulatedAnnealing<>(
					new NaturalBinaryDecoder(-100, 100, bits, 6),
					new BitVectorNeighborhood(),
					new BitVectorSolution(6),
					transferFunction,
					new GeometricTempSchedule(0.95, 1000000, 1000, 1000),
					true);
			algorithm.run();
		} else {
			System.out.println("Unknown method, expected 'decimal' or 'binary:x'");
			return;
		}

	}
	
	public static ReadingWrapper readFromFile(String fileName) {
		List<String> lines = null;
		try {
			lines = Files.readAllLines(Paths.get(fileName));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<List<Double>> x = new LinkedList<>(); 
		List<Double> y = new LinkedList<>();
		
		for (String line: lines) {
			if (line.startsWith("#")) {
				continue;
			}
			line = line.substring(1, line.length() - 1);
			String[] numbers = line.split(",");
			
			List<Double> xi = new ArrayList<>(numbers.length - 1);
			for (int i = 0; i < numbers.length - 1; i++) {
				xi.add(Double.parseDouble(numbers[i]));
				//xi.set(i, Double.parseDouble(numbers[i]));
			}
			x.add(xi);
			
			y.add(Double.parseDouble(numbers[numbers.length - 1]));
		}
		return new ReadingWrapper(x, y);
	}
	
	public static class ReadingWrapper {
		private List<List<Double>> x;
		private List<Double> y;
		
		public ReadingWrapper(List<List<Double>> x, List<Double> y) {
			this.x = x;
			this.y = y;
		}

		public List<List<Double>> getX() {
			return x;
		}

		public List<Double> getY() {
			return y;
		}
		
		
	}

}
