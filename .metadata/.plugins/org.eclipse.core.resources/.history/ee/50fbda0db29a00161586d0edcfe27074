package math.linear.defaults;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import math.linear.implementations.Matrix;
import math.linear.implementations.Vector;
import math.linear.interfaces.IMatrix;
import math.linear.interfaces.IVector;

/**
 * Class which offers static methods for creating default implementations of 
 * matrices and vectors.
 * @author ajuric
 *
 */
public class LinAlgDefaults {
	
	/**
	 * Creates new instance of matrix which is implementation of {@link Matrix} with given 
	 * parameters. 
	 * @param rows of new matrix
	 * @param cols of new matrix
	 * @return new matrix
	 */
	public static IMatrix defaultMatrix(int rows, int cols) {
		return new Matrix(rows, cols);
	}
	
	/**
	 * Creates new modifiable instance of vector which is implementation of {@link Vector}
	 * with given parameters.
	 * @param dimension of new vector
	 * @return new vector
	 */
	public static IVector defaultVector(int dimension) {
		return new Vector(false, true, new double[dimension]);
	}
	
	public static void writeToFile(IMatrix matrix, String fileName) throws FileNotFoundException {
		try(PrintWriter out = new PrintWriter(fileName)){
	    out.println(matrix);
		}
	}
	
	public static IMatrix readFromFileDefaultMatrix(String fileName) {
		List<String> lines = null;
		try {
			lines = Files.readAllLines(Paths.get(fileName));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return Matrix.parseSimple(String.join("|", lines));
		
		/*int rows = lines.size();
		int cols = 0;
		String separator = null;
		if (lines.get(0).contains("\t")) {
			separator = "\t";
			cols = lines.get(0).split(separator).length;
		} else {
			separator = "[\\s]+";
			cols = lines.get(0).split(separator).length;
		}
		
		IMatrix m = new Matrix(rows, cols);
		for (int i = lines.size() - 1; i >= 0; i--) {
			String[] row = lines.get(i).split(separator);
			if (row.length != cols) {
				throw new IllegalArgumentException("Matrix has different row sizes.");
			}
			for (int j = row.length - 1; j >= 0; j--) {
				m.set(i, j, Double.parseDouble(row[j]));
			}
		}
		return m;*/
	}

}
