package hr.fer.zemris.numer;

import java.util.List;

import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;

public class TransferFunction implements IHFunction {
	
	private List<RealVector> x;
	private List<Double> y;

	public TransferFunction(List<RealVector> x, List<Double> y) {
		this.x = x;
		this.y = y;
	}

	@Override
	public int getNumberOfVariables() {
		return 6;
	}

	@Override
	public double getFunctionValueAt(RealVector point) {
		double value = 0;
		for (int i = x.size() - 1; i >= 0; i++) {
			value += 
						Math.pow(errorDifference(point, i), 2);
		}
		return value;
	}
	
	private double errorDifference(RealVector point, int i) {
		double a = point.getEntry(0), b = point.getEntry(1), c = point.getEntry(2);
		double d = point.getEntry(3), e = point.getEntry(4), f = point.getEntry(5);
		double x1 = x.get(i).getEntry(0), x2 = x.get(i).getEntry(1), x3 = x.get(i).getEntry(2);
		double x4 = x.get(i).getEntry(3), x5 = x.get(i).getEntry(4);
		double diff = 
					Math.pow((a * x1
				+ b * Math.pow(x1, 3) * x2
				+ c * Math.pow(e, d * x3) * (1 + Math.cos(e * x4))
				+ f * x4 * Math.pow(x5, 2)) 
					-
				y.get(i), 2);
		return diff;
	}

	@Override
	public RealVector getGradientValueAt(RealVector point) {
		RealVector gradient = new ArrayRealVector(getNumberOfVariables());
		
		// derivative by a
		double gradA = 0;
		for (int i = x.size() - 1; i >= 0; i--) {
			gradA += errorDifference(point, i) * x.get(i).getEntry(0);
		}
		gradient.setEntry(0, 2 * gradA);
		
		// derivative by b
		double gradB = 0;
		for (int i = x.size() - 1; i >= 0; i--) {
			gradB += errorDifference(point, i) * Math.pow(x.get(i).getEntry(0), 2) * x.get(i).getEntry(1);
		}
		gradient.setEntry(1, 2 * gradB);
		
		// derivative by c
		double gradC = 0;
		for (int i = x.size() - 1; i >= 0; i--) {
			gradC += errorDifference(point, i) * Math.pow(point.getEntry(4), point.getEntry(3) * x.get(i).getEntry(2)) * (1 + Math.cos(point.getEntry(4) * x.get(i).getEntry(3)));
		}
		gradient.setEntry(2, 2 * gradC);
		
		// derivative by d
		double gradD = 0;
		for (int i = x.size() - 1; i >= 0; i--) {
			gradD += errorDifference(point, i) * Math.pow(point.getEntry(4), point.getEntry(3) * x.get(i).getEntry(2)) * (1 + Math.cos(point.getEntry(4) * x.get(i).getEntry(3)));
		}
		gradient.setEntry(3, 2 * gradD);
		
		// derivative by e
		double gradE = 0;
		for (int i = x.size() - 1; i >= 0; i--) {
			gradE += errorDifference(point, i) * (
					x.get(i).getEntry(2) * point.getEntry(2) * point.getEntry(3) * Math.pow(point.getEntry(4), x.get(i).getEntry(2) * point.getEntry(3) - 1)
				-	x.get(i).getEntry(3) * point.getEntry(2) * Math.pow(point.getEntry(4), x.get(i).getEntry(2) * point.getEntry(3)) * Math.sin(x.get(i).getEntry(3) * point.getEntry(4)));
		}
		gradient.setEntry(4, 2 * gradE);
		
		
		return gradient;
	}

	@Override
	public RealMatrix getHesseMatrix(RealVector point) {
		return null;
		// Not implemented yet. Try this:
		// https://www.wolframalpha.com/input/?i=hessian+(sum(a+*+x_i+%2B+b+*+x_i%5E3+*+y_i+%2B+c+*+e+%5E+(d+*+z_i)+*+(1+%2B+cos(e+*+v_i))+%2B+f+*+v_i+*+u_i%5E2),+i+%3D+1+to+6)+with+respect+to+(a,+b,+c,+d,+e,+f)
	}

}
