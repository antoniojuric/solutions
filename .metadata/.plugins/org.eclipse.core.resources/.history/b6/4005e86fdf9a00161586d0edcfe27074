package math.linear.models;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import math.linear.exceptions.IncompatibleOperandException;
import math.linear.implementations.MatrixSubMatrixView;
import math.linear.implementations.MatrixTransposeView;
import math.linear.implementations.VectorMatrixView;
import math.linear.interfaces.IMatrix;
import math.linear.interfaces.IVector;
import math.linear.wrappers.LUPWrapper;

/**
 * Implementation of {@link IMatrix} interface.
 * Implements methods which are independent from matrix implementation. 
 * @author ajuric
 *
 */
public abstract class AbstractMatrix implements IMatrix {
	
	/**
	 * For comparing real numbers.
	 */
	private static final double DELTA = 1e-7;
	
	@Override
	public abstract int getRowsCount();

	@Override
	public abstract int getColsCount();

	@Override
	public abstract double get(int row, int col);

	@Override
	public abstract IMatrix set(int row, int col, double value);

	@Override
	public abstract IMatrix copy();

	@Override
	public abstract IMatrix newInstance(int rows, int cols);

	@Override
	public IMatrix nTranspose(boolean liveView) {
		return new MatrixTransposeView(liveView ? this : this.copy());
	}

	@Override
	public IMatrix add(IMatrix other) {
		return this.doOperation(other, (value1, value2) -> value1 + value2);
	}

	@Override
	public IMatrix nAdd(IMatrix other) {
		return this.copy().add(other);
	}

	@Override
	public IMatrix sub(IMatrix other) {
		return this.doOperation(other, (value1, value2) -> value1 - value2);
	}

	@Override
	public IMatrix nSub(IMatrix other) {
		return this.copy().sub(other);
	}

	/**
	 * @throws IncompatibleOperandException if matrices are not compatible for 
	 * multiplication
	 */
	@Override
	public IMatrix nMultiply(IMatrix other) {
		if (this.getColsCount() != other.getRowsCount()) {
			throw new IncompatibleOperandException("Matrices are not compatible for "
				+ "multiplication. Expected matrices dimenstions: [n, p] × [p, m] = [n, m].");
		}
		IMatrix newMatrix = this.newInstance(this.getRowsCount(), other.getColsCount());
		
		for (int i = 0, 
			rows = this.getRowsCount(), cols = other.getColsCount(), iter = this.getColsCount(); i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				double tmpSum = 0;
				for (int k = 0; k < iter; k++) {
					tmpSum += this.get(i, k) * other.get(k, j);
				}
				newMatrix.set(i, j, tmpSum);
			}
		}
		
		return newMatrix;
	}

	/**
	 * This calculation of determinant implements 
	 * <a href="http://en.wikipedia.org/wiki/Laplace_expansion">Laplace expansion</a>.
	 */
	@Override
	public double determinant() throws IncompatibleOperandException {
		if (this.getRowsCount() != this.getColsCount()){
			throw new IncompatibleOperandException("Determinants are defined only "
					+ "on square matrices.");
		}
		int rows = this.getRowsCount();
		if (rows == 1) {
			return this.get(0, 0);
		} else if (rows == 2) {
			return this.get(0, 0) * this.get(1, 1) - this.get(0, 1) * this.get(1, 0);
		} else if (rows == 3) {
			return threeDimDeterminant();
		} else {
			double determinant = 0;
			int sign = 1;
			for (int i = 0, cols = this.getColsCount(); i < cols; i++) {
				determinant += sign * this.get(0, i) * (new MatrixSubMatrixView(this, 0, i)).determinant();
				sign *= -1;
			}
			
			return determinant;
		}
	}
	
	/**
	 * Calculates determinant with exact formula only of matrices with size 3 * 3.
	 * @return determinant
	 */
	private double threeDimDeterminant() {
		double a = this.get(0, 0), b = this.get(0, 1), c = this.get(0, 2);
		double d = this.get(1, 0), e = this.get(1, 1), f = this.get(1, 2);
		double g = this.get(2, 0), h = this.get(2, 1), i = this.get(2, 2);
		return a*e*i + b*f*g + c*d*h - c*e*g - b*d*i - a*f*h; 
	}
	
	@Override
	public IMatrix subMatrix(int row, int col, boolean liveView) {
		return new MatrixSubMatrixView(liveView ? this : this.copy(), row, col);
	}
	
	/**
	 * This implementation of inverting matrix uses Cramer's rule for computation.
	 * See: <a href="http://en.wikipedia.org/wiki/Invertible_matrix#Analytic_solution">
	 * Analytic solution </a>.
	 * @return new instance of matrix which is inverted matrix of current matrix
	 * @throws IncompatibleOperandException if given matrix is singular
	 */
	@Override
	public IMatrix nInvert() {
		IMatrix inversMatrix = this.copy();
		double determinant = this.determinant();
		if (Math.abs(determinant) < DELTA) {
			throw new IncompatibleOperandException("Matrix is sinuglar: singular matrices"
					+ "don't have inverse matrix.");
		}
		int rowSign = 1;
		for (int i = 0, rows = this.getRowsCount(), cols = this.getColsCount(); i < rows; i++) {
			int sign = rowSign;
			for (int j = 0; j < cols; j++) {
				inversMatrix.set(i, j, sign * (new MatrixSubMatrixView(this, i, j)).determinant());
				sign *= -1;
			}
			rowSign *= -1;
		}
		return inversMatrix.nTranspose(true).scalarMultiply(1 / determinant);
	}

	@Override
	public double[][] toArray() {
		double[][] array = new double[this.getRowsCount()][this.getColsCount()];
		for (int i = this.getRowsCount() - 1; i >= 0; i--) {
			for (int j = this.getColsCount() - 1; j >= 0; j--) {
				array[i][j] = this.get(i, j);
			}
		}
		return array;
	}

	@Override
	public IVector toVector(boolean liveView) {
		return new VectorMatrixView(liveView ? this : this.copy());
	}

	@Override
	public IMatrix nScalarMultiply(double value) {
		return this.copy().scalarMultiply(value);
	}

	@Override
	public IMatrix scalarMultiply(double value) {
		for (int i = this.getRowsCount() - 1; i >= 0; i--) {
			for (int j = this.getColsCount() - 1; j >= 0; j--) {
				this.set(i, j, this.get(i, j) * value);
			}
		}
		return this;
	}

	/**
	 * Creates identity matrix of current matrix. All values in matrix are overriden by identity matrix.
	 * @return identity matrix with size of current matrix
	 * @throws IncompatibleOperandException if current matrix is not square matrix
	 */
	public IMatrix makeIdentity() {
		if (this.getRowsCount() != this.getColsCount()) {
			throw new IncompatibleOperandException("Current matrix is not square matrix. Therefore,"
					+ " it cannot be identity matrix.");
		}
		for (int i = this.getRowsCount() - 1; i >= 0; i--) {
			for (int j = this.getColsCount() - 1; j >= 0; j--) {
				this.set(i, j, i == j ? 1 : 0);
			}
		}
		return this;
	}
	
	@Override
	public IMatrix decompositionLU() {
		if (getRowsCount() != getColsCount()) {
			throw new IncompatibleOperandException("LU decomposition supported only for square matrices.");
		}
		for (int i = 0, n = getRowsCount(); i < n - 1; i++) {
			for (int j = i + 1; j < n; j++) {
				System.out.println(this.get(i, i));
				if (Math.abs(get(i, i)) < DELTA) {
					throw new IllegalArgumentException("Division with zero - diagonal element is zero");
				}
				this.set(j, i, this.get(j, i) / this.get(i, i));
				for (int k = i + 1; k < n; k++) {
					this.set(j, k, this.get(j, k) - this.get(j, i) * this.get(i, k));
				}
			}
		}
		return this;
	}
	
	@Override
	public IMatrix nDecompositionLU() {
		return this.copy().decompositionLU();
	}
	
	@Override
	public LUPWrapper decompositionLUP() {
		int n = this.getRowsCount();
		int[] P = new int[n];
		for (int i = 0; i < n; i++) {
			P[i] = i;
		}
		for (int i = 0; i < n - 1; i++) {
			int pivot = i;
			for (int j = i + 1; j < n; j++) {
				if (Math.abs(this.get(P[j], i)) > Math.abs(this.get(P[pivot], i))) {
					pivot = j;
				}
			}
			{ // swap
				int tmp = P[i];
				P[i] = P[pivot];
				P[pivot] = tmp;
			}
			for (int j = i + 1; j < n; j++) {
				if (Math.abs(this.get(P[i], i)) < DELTA) {
					throw new IllegalArgumentException("Division with zero - diagonal element is zero");
				}
				this.set(P[j], i, this.get(P[j], i) / this.get(P[i], i));
				for (int k = i + 1; k < n; k++) {
					this.set(P[j], k, this.get(P[j], k) - this.get(P[j], i) * this.get(P[i], k));
				}
			}
		}
		return new LUPWrapper(this, P);
	}
	
	@Override
	public IMatrix nPermuteRows(int[] P) {
		if (P.length != getRowsCount()) {
			throw new IllegalArgumentException("Permutation array and matrix are not of same size.");
		}
		IMatrix permutedMatrix = this.newInstance(this.getRowsCount(), this.getColsCount());
		for (int i = 0, rows = getRowsCount(), cols = getColsCount(); i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				permutedMatrix.set(P[i], j, this.get(i, j));
			}
		}
		return permutedMatrix;
	}
	
	@Override
	public LUPWrapper nDecompositionLUP() {
		return this.copy().decompositionLUP();
	}
	
	@Override
	public IMatrix substitutionForward(IMatrix b) {
		if (this.getRowsCount() != this.getColsCount()) {
			throw new IncompatibleOperandException("Forward substitution supported only for square matrices.");
		}
		IVector bVector = new VectorMatrixView(b);
		for (int i = 0, n = this.getRowsCount(); i < n - 1; i++) {
			for (int j = i + 1; j < n; j++) {
				bVector.set(j, bVector.get(j) - this.get(j, i) * bVector.get(i));
			}
		}
		return b;
	}
	
	@Override
	public IMatrix nSubstitutionForward(IMatrix b) {
		return substitutionForward(b.copy());
	}
	
	@Override
	public IMatrix substitutionBackward(IMatrix b) {
		if (this.getRowsCount() != this.getColsCount()) {
			throw new IncompatibleOperandException("Forward substitution supported only for square matrices.");
		}
		IVector bVector = new VectorMatrixView(b);
		for (int n = this.getRowsCount(), i = n - 1; i >= 0; i--) {
			if (Math.abs(get(i, i)) < DELTA) {
				throw new IllegalArgumentException("Division with zero - diagonal element is zero");
			}
			bVector.set(i, bVector.get(i) / this.get(i, i));
			for (int j = 0; j < i - 1; j++) {
				bVector.set(j, bVector.get(j) - this.get(j, i) * bVector.get(i));
			}
		}
		return b;
	}
	
	@Override
	public IMatrix nSubstitutionBackward(IMatrix b) {
		return substitutionBackward(b.copy());
	}
	
	/**
	 * Private interface used for modeling basic operations execution like addition,
	 * subtraction, ...
	 * @author ajuric
	 *
	 */
	private interface IOperation {
		/**
		 * Execute defined operation.
		 * @param d1 first value
		 * @param d2 second value
		 * @return result of operation over <code>d1</code> and <code>d2</code>
		 */
		public double execOperation(double d1, double d2);
	}
	
	/**
	 * Execute given operation and modify current matrix.
	 * @param other second operand
	 * @param operation to be executed, instance of {@link IOperation}s
	 * @return resulting matrix (current matrix modified)
	 * @throws IncompatibleOperandException if matrices are not of equal sizes
	 */
	private IMatrix doOperation(IMatrix other, IOperation operation){
		if (this.getRowsCount() != other.getRowsCount() ||
			this.getColsCount() != other.getColsCount()) {
				throw new IncompatibleOperandException("Matrices must be of equal sizes.");
		}
			
		for (int i = this.getRowsCount() - 1; i >= 0; i--) {
			for (int j = this.getColsCount() - 1; j >= 0; j--){
				this.set(i, j, operation.execOperation(this.get(i, j), other.get(i, j)));
			}
		}
		
		return this;
	}
	
	@Override
	public String toString() {
		return this.toString(3);
	}
	
	/**
	 * Formats current matrix in a way that every number is printed with given precision.
	 * @param precision of matrix values
	 * @return string representation of current object
	 */
	public String toString(int precision) {
		PrintStream oldStream = System.out;
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		PrintStream printStream = new PrintStream(baos);
		System.setOut(printStream);
		for (int i = 0, rows = this.getRowsCount(), cols = this.getColsCount(); i < rows; i++) {
			System.out.format("[");
			for (int j = 0; j < cols; j++) {
				double value = this.get(i, j);
				System.out.format((value < 0 ? "" : " ") + "%."+precision+"f", this.get(i, j));
				if (j < cols - 1) {
					System.out.format(", ");
				}
			}
			System.out.format("]");
			if (i < rows  - 1) {
				System.out.format("%n");
			}
		}
		
		System.out.flush();
		System.setOut(oldStream);
		
		return baos.toString();
	}


}
