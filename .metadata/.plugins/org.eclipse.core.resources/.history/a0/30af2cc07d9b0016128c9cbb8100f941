package hr.fer.zemris.fuzzy;

import java.util.HashSet;
import java.util.Set;

public class Relations {
	
	private static final double DELTA = 1e-6;
	
	public static boolean isUtimesURelation(IFuzzySet relation) {
		if (relation.getDomain().getNumberOfComponents() != 2) { // only U x U
			return false;
		}
		Set<Integer> u1 = new HashSet<>();
		Set<Integer> u2 = new HashSet<>();
		
		for (DomainElement element : relation.getDomain()) {
			u1.add(element.getComponentValue(0));
			u2.add(element.getComponentValue(1));
		}
		
		return u1.equals(u2);
	}
	
	public static boolean isSymmetric(IFuzzySet relation) {
		if (!isUtimesURelation(relation)) {
			return false;
		}
		
		for (DomainElement element : relation.getDomain()) {
			if (!compare(
					relation.getValueAt(element), relation.getValueAt(DomainElement.invert(element)))) {
				return false;
			}
		}
		
		return true;
	}
	
	public static boolean isReflexive(IFuzzySet relation) {
		if (!isUtimesURelation(relation)) {
			return false;
		}
		
		int first = ((SimpleDomain)relation.getDomain().getComponent(0)).getFirst();
		int last = ((SimpleDomain)relation.getDomain().getComponent(0)).getLast();
		
		for (int i = first; i < last; i++) {
			if (!compare(relation.getValueAt(DomainElement.of(i, i)), 1)) {
				return false;
			}
		}
		
		return true;
	}
	
	public static boolean isMaxMinTransitive(IFuzzySet relation) {
		if (!isUtimesURelation(relation)) {
			return false;
		}
		
		int first = ((SimpleDomain)relation.getDomain().getComponent(0)).getFirst();
		int last = ((SimpleDomain)relation.getDomain().getComponent(0)).getLast();
		
		for (int x = first; x < last; x++) {
			for (int z = first; z < last; z++) {
				/*if (z == x) {
					continue;
				}*/
				double value = 0;
				for (int y = first; y < last; y++) {
					if (y == z && y == x) {
						continue;
					}
					value = Math.max(value, 
							Math.min(
									relation.getValueAt(DomainElement.of(x, y)),
									relation.getValueAt(DomainElement.of(y, z))
							));
				}
				double tmp = relation.getValueAt(DomainElement.of(x, z));
				if (value < relation.getValueAt(DomainElement.of(x, z))) {
					return false;
				}
			}
		}
		
		return true;
	}
	
	private static boolean compare(double d1, double d2) {
		return Math.abs(d1 - d2) < DELTA;
	}

}
