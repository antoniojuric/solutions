package hr.fer.zemris.java.dz3;

public class GreedyAlgorithm<T extends SingleObjectiveSolution> implements IOptAlgorithm<T> {
	
	private static final int ITERATION_LIMIT = 10_000;
	
	private IDecoder<T> decoder;
	private INeighborhood<T> neighborhood;
	private T startWith;
	private IFunction function;
	private boolean minimize;

	public GreedyAlgorithm(IDecoder<T> decoder, INeighborhood<T> neighborhood, T startWith,
			IFunction function, boolean minimize) {
		this.decoder = decoder;
		this.neighborhood = neighborhood;
		this.startWith = startWith;
		this.function = function;
		this.minimize = minimize;
	}

	@Override
	public T run() {
		T solution = startWith;
		solution.setValue(function.valueAt(decoder.decode(solution)));
		
		for (int i = 0; i < ITERATION_LIMIT; i++) {
			T neighbor = neighborhood.randomNeighbor(solution);
			neighbor.setValue(function.valueAt(decoder.decode(neighbor)));
			if (minimize) { // minimize
				if (solution.compareTo(neighbor) > 0) {
					solution = neighbor;
				}
			} else { // maximize
				if (solution.compareTo(neighbor) < 0) {
					solution = neighbor;
				}
			}
		}
		
		System.out.println("Found solution: " + solution.getValue() + "at:" + 
				printSolution(solution, decoder));
	
		return solution;
	}

}
