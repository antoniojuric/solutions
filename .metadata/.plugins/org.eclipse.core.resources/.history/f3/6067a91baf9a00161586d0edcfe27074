package math.linear.defaults;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import math.linear.implementations.Matrix;
import math.linear.implementations.Vector;
import math.linear.interfaces.IMatrix;
import math.linear.interfaces.IVector;

/**
 * Class which offers static methods for creating default implementations of 
 * matrices and vectors.
 * @author ajuric
 *
 */
public class LinAlgDefaults {
	
	/**
	 * Creates new instance of matrix which is implementation of {@link Matrix} with given 
	 * parameters. 
	 * @param rows of new matrix
	 * @param cols of new matrix
	 * @return new matrix
	 */
	public static IMatrix defaultMatrix(int rows, int cols) {
		return new Matrix(rows, cols);
	}
	
	/**
	 * Creates new modifiable instance of vector which is implementation of {@link Vector}
	 * with given parameters.
	 * @param dimension of new vector
	 * @return new vector
	 */
	public static IVector defaultVector(int dimension) {
		return new Vector(false, true, new double[dimension]);
	}
	
	public static IMatrix readFromFile(String fileName) {
		List<String> lines = Files.readAllLines(Paths.get(fileName));
	}

}
