package hr.fer.zemris.numer;

import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.LUDecomposition;
import org.apache.commons.math3.linear.RealVector;
import org.apache.commons.math3.random.RandomVectorGenerator;
import org.apache.commons.math3.random.SobolSequenceGenerator;

public class NumOptAlgorithms {
	
	private static final double ZERO = 1e-6;
	
	private static RandomVectorGenerator generator;
	private static int dimension;
	
	public static RealVector calculateGradientDescent(IFunction f, int iterationLimit, 
			RealVector startSolution) {
		RealVector x = startSolution;
		System.out.println("Gradient descent for " + f + ", iteration limit:" + iterationLimit);
		System.out.println("Starting point: " + x);
		for (int k = 0; k < iterationLimit; k++) {
			System.out.println("Iteration number: " + k + ",\t currentValue: " + x);
			if (Math.abs(f.getGradientValueAt(x).getNorm()) < ZERO) {
				return x; // solution found
			}
			RealVector d = f.getGradientValueAt(x).mapMultiply(-1);
			double lambda = findLambda(f, x, d);
			x = x.add(d.mapMultiplyToSelf(lambda));
		}
		return null; // solution not found
	}
	
	public static RealVector calculateGradientDescent(IFunction f, int iterationLimit) {
		return calculateGradientDescent(
				f, iterationLimit, generateRandomVector(f.getNumberOfVariables()));
	}
	
	// returns minimum calculated by gradient descent
	public static RealVector calculateGradientDescent(IFunction f, int iterationLimit, Double x0, Double y0) {
		RealVector x;
		if (x0 == null) {
			x = new ArrayRealVector(new double[]{5, 5});
		} else {
			x = new ArrayRealVector(new double[]{x0, y0});
		}
		return calculateGradientDescent(f, iterationLimit, x);
	}
	
	public static RealVector calculateNewtonsMethod(IHFunction f, int iterationLimit RealVector startSolution) {
		RealVector x = startSolution;
		System.out.println("Newton method for " + f + ", iteration limit:" + iterationLimit);
		System.out.println("Starting point: " + x);
		for (int k = 0; k < iterationLimit; k++) {
			System.out.println("Iteration number: " + k + ",\t currentValue: " + x);
			if (Math.abs(f.getGradientValueAt(x).getNorm()) < ZERO) {
				return x; // solution found
			}
			RealVector d = new LUDecomposition(f.getHesseMatrix(x)).getSolver().getInverse()
					.operate(f.getGradientValueAt(x)).mapMultiply(-1);
			x = x.add(d);
		}
		return null; // solution not found
	}
	
	public static RealVector calculateNewtonsMethod(IHFunction f, int iterationLimit, Double x0, Double y0) {
		//RealVector x = generateRandomVector(f.getNumberOfVariables());
		RealVector x;
		if (x0 == null) {
			x = new ArrayRealVector(new double[]{5, 5});
		} else {
			x = new ArrayRealVector(new double[]{x0, y0});
		}
	}
	
	private static double findLambda(IFunction f, RealVector x0, RealVector d0) {
		double lambaLower = 0, lambaUpper = 0.001;
		while(true) {
			if (dtheta(f, x0, d0, lambaUpper) > 0) {
				break;
			}
			lambaLower = lambaUpper;
			lambaUpper *= 2;
		}
		
		double lambdaLo = lambaLower, lambdaHi = lambaUpper;
		double lambda;
		while(true) {
			lambda = (lambdaLo + lambdaHi) / 2;
			double dthetaValue = dtheta(f, x0, d0, lambda);
			if (Math.abs(dthetaValue) < ZERO) {
				break;
			} else if (dthetaValue > 0) {
				lambdaHi = lambda;
			} else {
				lambdaLo = lambda;
			}
		}
		return lambda;
	}

	private static double dtheta(IFunction f, RealVector x0, RealVector d0, double lambda) {
		return f.getGradientValueAt(x0.add(d0.mapMultiply(lambda))).dotProduct(d0);
	}

	private static RealVector generateRandomVector(int dimension) {
		if (generator == null) {
			generator = new SobolSequenceGenerator(dimension); 
		} else if (NumOptAlgorithms.dimension != dimension) {
			generator = new SobolSequenceGenerator(dimension);
		}
		NumOptAlgorithms.dimension = dimension;
		return new ArrayRealVector(generator.nextVector());
	}

}
