package math.linear.interfaces;

import math.linear.wrappers.LUPWrapper;

/**
 * Interface for mathematical matrix object.
 * @author ajuric
 *
 */
public interface IMatrix {
	
	/**
	 * Gives number of rows of current matrix.
	 * @return number of rows
	 */
	public int getRowsCount();
	/**
	 * Gives number of columns of current matrix.
	 * @return number of columns
	 */
	public int getColsCount();
	/**
	 * Gives value of matrix at position <code>[row, col]</code>.
	 * @param row index
	 * @param col index
	 * @return value at given position
	 */
	public double get(int row, int col);
	/**
	 * Sets value of matrix at position <code>[row, col]</code> to given <code>value</code>.
	 * @param row index
	 * @param col index
	 * @param value to be set
	 * @return reference to same modified matrix
	 */
	public IMatrix set(int row, int col, double value);
	/**
	 * Creates new instance of {@link IMatrix} with values same as current matrix, ie. exact copy.
	 * @return copy of current matrix
	 */
	public IMatrix copy();
	/**
	 * Creates new instance of {@link IMatrix} with size <code>rows * cols</code> filled with zeros. 
	 * @param rows number of rows
	 * @param cols number of columns
	 * @return new zero filled matrix
	 */
	public IMatrix newInstance(int rows, int cols);
	/**
	 * Returns transposed view of given matrix. If <code>liveView</code> is set to <code>true</code>, modifications are
	 * done on current matrix also because newly transposed matrix holds reference to it.
	 * @param liveView indicates is this live views
	 * @return transposed matrix
	 */
	public IMatrix nTranspose(boolean liveView);
	public IMatrix add(IMatrix other);
	public IMatrix nAdd(IMatrix other);
	public IMatrix sub(IMatrix other);
	public IMatrix nSub(IMatrix other);
	public IMatrix nMultiply(IMatrix other);
	public IMatrix nScalarMultiply(double value);
	public IMatrix scalarMultiply(double value);
	public double determinant();
	public IMatrix subMatrix(int row, int col, boolean liveView);
	public IMatrix nInvert();
	public double[][] toArray();
	public IVector toVector(boolean liveView);
	public IMatrix decompositionLU();
	public IMatrix nDecompositionLU();
	public IMatrix substitutionForward(IMatrix b);
	public IMatrix nSubstitutionForward(IMatrix b);
	public IMatrix substitutionBackward(IMatrix b);
	public IMatrix nSubstitutionBackward(IMatrix b);
	public LUPWrapper decompositionLUP();
	public LUPWrapper nDecompositionLUP();
	public IMatrix permuteRows(int[] P);
	public IMatrix nPermuteRows(int[] P);

}
