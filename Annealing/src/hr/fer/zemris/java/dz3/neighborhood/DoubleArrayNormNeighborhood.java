package hr.fer.zemris.java.dz3.neighborhood;

import java.util.Random;

import hr.fer.zemris.java.dz3.solution.DoubleArraySolution;

public class DoubleArrayNormNeighborhood implements INeighborhood<DoubleArraySolution> {
	
	private double[] deltas;	
	private Random rand;

	public DoubleArrayNormNeighborhood(double[] deltas) {
		this.deltas = deltas;
		this.rand = new Random();
	}

	@Override
	public DoubleArraySolution randomNeighbor(DoubleArraySolution current) {
		DoubleArraySolution neighbor = current.duplicate();
		for (int i = current.getDimension() - 1; i >= 0; i--) {
			neighbor.setValue(i, neighbor.getValue(i) + rand.nextGaussian() * deltas[i]);
		}
		return neighbor;
	}

}
