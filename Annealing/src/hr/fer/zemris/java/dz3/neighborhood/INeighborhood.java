package hr.fer.zemris.java.dz3.neighborhood;

import hr.fer.zemris.java.dz3.solution.SingleObjectiveSolution;

public interface INeighborhood <T extends SingleObjectiveSolution> {
	
	public T randomNeighbor(T current);

}
