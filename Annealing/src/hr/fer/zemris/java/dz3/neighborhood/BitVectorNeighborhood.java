package hr.fer.zemris.java.dz3.neighborhood;

import java.util.Random;

import hr.fer.zemris.java.dz3.solution.BitVectorSolution;

public class BitVectorNeighborhood implements INeighborhood<BitVectorSolution> {
	
	private static double MUTATION_BIT_CHANCE = 0.10;

	private Random rand;

	public BitVectorNeighborhood() {
		this.rand = new Random();
	}

	@Override
	public BitVectorSolution randomNeighbor(BitVectorSolution current) {
		BitVectorSolution neighbor = current.duplicate();
		for (int i = current.getLenght() - 1; i >= 0; i--) {
			if (rand.nextDouble() < MUTATION_BIT_CHANCE) {
				neighbor.setBitAt(i, (byte) (1 - neighbor.getBitAt(i)));
			}
		}
		return neighbor;
	}

}
