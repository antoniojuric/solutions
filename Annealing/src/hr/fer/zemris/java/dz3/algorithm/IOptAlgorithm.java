package hr.fer.zemris.java.dz3.algorithm;

import hr.fer.zemris.java.dz3.solution.SingleObjectiveSolution;

public interface IOptAlgorithm <T extends SingleObjectiveSolution> {
	
	public T run();

}
