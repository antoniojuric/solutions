package hr.fer.zemris.java.dz3.decoder;

import hr.fer.zemris.java.dz3.solution.BitVectorSolution;

public class NaturalBinaryDecoder extends BitVectorDecoder {
	
	public NaturalBinaryDecoder(double min, double max, int bits, int n) {
		super(min, max, bits, n);
	}
	
	public NaturalBinaryDecoder(double[] mins, double maxs[], int bits[], int n) {
		super(mins, maxs, bits, n);
	}

	@Override
	public double[] decode(BitVectorSolution code) {
		double[] decoded = new double[n];
		decode(code, decoded);
		return decoded;
	}

	@Override
	public void decode(BitVectorSolution code, double[] decoded) {
		int passedBits = 0;
		for (int i = 0; i < n; i++) {
			int k = 0;
			for (int j = 0; j < bits[i]; j++) {
				k += code.getBitAt(passedBits + j) << j;
			}
			double value = mins[i] + (k / (double) ((1 << bits[i]) - 1)) * (maxs[i] - mins[i]);
			decoded[i] = value;
			
			passedBits += bits[i];
		}
	}

}
