package hr.fer.zemris.java.dz3.decoder;

import hr.fer.zemris.java.dz3.solution.SingleObjectiveSolution;

public interface IDecoder <T extends SingleObjectiveSolution> {
	
	public double[] decode(T code);
	
	public void decode(T code, double[] decoded);
	
	public String printCode(T code);

}
