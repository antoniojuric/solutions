package hr.fer.zemris.java.dz3.decoder;

import java.util.Arrays;

import hr.fer.zemris.java.dz3.solution.BitVectorSolution;

public abstract class BitVectorDecoder extends Decoder<BitVectorSolution> {
	
	protected double[] mins;
	protected double[] maxs;
	protected int[] bits;
	protected int n; // dimension: number of variables
	protected int totalBits;
	
	public BitVectorDecoder(double min, double max, int bits, int n) {
		this.maxs = new double[n];
		this.mins = new double[n];
		this.bits = new int[n];
		for (int i = 0; i < n; i++) {
			maxs[i] = max;
			mins[i] = min;
			this.bits[i] = bits;
		}
		
		this.n = n;
		this.totalBits = bits * n;
	}
	
	public BitVectorDecoder(double[] mins, double maxs[], int bits[], int n) {
		if (mins.length != n || maxs.length != n || bits.length != n) {
			throw new IllegalArgumentException("Given arrays must be equal to given n.");
		}
		
		this.maxs = Arrays.copyOf(maxs, maxs.length);
		this.mins = Arrays.copyOf(mins, mins.length);
		this.bits = Arrays.copyOf(bits, bits.length);
		this.n = n;
		
		for (int i = 0; i < n; i++) {
			this.totalBits += bits[i];
		}
	}
	
	public int getTotalBits() {
		return totalBits;
	}
	public int getDimension() {
		return n;
	}

	@Override
	public abstract double[] decode(BitVectorSolution code);

	@Override
	public abstract void decode(BitVectorSolution code, double[] decoded);

}
