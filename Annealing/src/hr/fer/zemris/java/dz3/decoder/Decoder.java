package hr.fer.zemris.java.dz3.decoder;

import hr.fer.zemris.java.dz3.solution.SingleObjectiveSolution;

public abstract class Decoder<T extends SingleObjectiveSolution> implements IDecoder<T> {
	
	public String printCode(T code) {
		double decoded[] = decode(code);
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append('[');
		for (int i = 0; i < decoded.length; i++) {
			stringBuilder.append(decoded[i]);
			if (i < decoded.length - 1) {
				stringBuilder.append(", ");
			}
		}
		return stringBuilder.append(']').toString();
	}

}
