package hr.fer.zemris.java.dz3.decoder;

import hr.fer.zemris.java.dz3.solution.DoubleArraySolution;

public class PassThroughDecoder extends Decoder<DoubleArraySolution> {

	@Override
	public double[] decode(DoubleArraySolution code) {
		double[] decoded = new double[code.getDimension()];
		decode(code, decoded);
		return decoded;
	}

	@Override
	public void decode(DoubleArraySolution code, double[] decoded) {
		for (int i = code.getDimension() - 1; i >= 0; i--) {
			decoded[i] = code.getValue(i);
		}
		
	}


}
