package hr.fer.zemris.java.dz3.tempschedule;

public class GeometricTempSchedule implements ITempSchedule {
	
	private double alpha;
	private double tInitial;
	private double tCurrent;
	private int innerLimit;
	private int outerLimit;

	public GeometricTempSchedule(double alpha, double tInitial, int innerLimit, int outerLimit) {
		this.alpha = alpha;
		this.tInitial = tInitial;
		this.innerLimit = innerLimit;
		this.outerLimit = outerLimit;
		
		tCurrent = this.tInitial;
	}

	@Override
	public double getNextTemperature() {
		double tReturned = tCurrent;
		tCurrent *= alpha;
		return tReturned;
	}

	@Override
	public int getInnerLoopCounter() {
		return innerLimit;
	}

	@Override
	public int getOuterLoopCounter() {
		return outerLimit;
	}

}
