package hr.fer.zemris.java.dz3.tempschedule;

public interface ITempSchedule {
	
	public double getNextTemperature();
	public int getInnerLoopCounter();
	public int getOuterLoopCounter();

}
