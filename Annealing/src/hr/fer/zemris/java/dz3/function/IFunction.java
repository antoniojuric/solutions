package hr.fer.zemris.java.dz3.function;

public interface IFunction {
	
	public double valueAt(double[] point);

}
