package hr.fer.zemris.java.dz3.function;

public class TestF2 implements IFunction {

	/**
	 * f2(x1, x2) = (x1 - 1)^2 + 10*(x2 - 2)^2
	 */
	@Override
	public double valueAt(double[] point) {
		return Math.pow(point[0] - 1, 2) + 10*Math.pow(point[1] - 2, 2);
	}

}
