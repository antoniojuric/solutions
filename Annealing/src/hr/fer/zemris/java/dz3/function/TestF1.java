package hr.fer.zemris.java.dz3.function;

public class TestF1 implements IFunction {

	/**
	 * f1(x1, x2) = x1^2 + (x2 - 1)^2
	 */
	@Override
	public double valueAt(double[] point) {
		return Math.pow(point[0], 2) + Math.pow(point[1] - 1, 2);
	}

}
