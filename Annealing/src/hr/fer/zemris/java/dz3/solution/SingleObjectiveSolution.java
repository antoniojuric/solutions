package hr.fer.zemris.java.dz3.solution;

public class SingleObjectiveSolution implements Comparable<SingleObjectiveSolution> {
	
	private double fitness;
	private double value;
	
	public SingleObjectiveSolution() { }
	
	public void setValue(double value) {
		this.value = value;
	}

	public double getValue() {
		return value;
	}

	@Override
	public int compareTo(SingleObjectiveSolution other) {
		return Double.compare(this.value, other.value);
	}
	
	

}
