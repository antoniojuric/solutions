package hr.fer.zemris.java.dz3.solution;

import java.util.Random;

public class BitVectorSolution extends SingleObjectiveSolution {
	
	private byte[] bits;
	
	public BitVectorSolution(int size) {
		this.bits = new byte[size];
	}
	
	public byte getBitAt(int index) {
		return bits[index];
	}
	
	public void setBitAt(int index, byte bit) {
		bits[index] = bit;
	}
	
	public int getLenght() {
		return bits.length;
	}
	
	public BitVectorSolution newLikeThis() {
		return new BitVectorSolution(bits.length);
	}
	
	public BitVectorSolution duplicate() {
		BitVectorSolution duplicate = newLikeThis();
		for (int i = bits.length - 1; i >= 0; i--) {
			duplicate.bits[i] = this.bits[i];
		}
		return duplicate;
	}
	
	public void randomize(Random random) {
		// TODO
	}

}
