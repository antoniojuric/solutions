package hr.fer.zemris.optjava.dz2;

import org.apache.commons.math3.linear.RealVector;

import hr.fer.zemris.numer.IFunction;
import hr.fer.zemris.numer.IHFunction;
import hr.fer.zemris.numer.NumOptAlgorithms;
import hr.fer.zemris.numer.TransferFunction;
import hr.fer.zemris.optjava.dz2.Sustav.ReadingWrapper;

public class Prijenosna {
	
	public static void main(String[] args) {
		if (args.length != 3) {
			System.out.println("3 arguments expected: method-name number-of-iterations filename");
			return;
		}
		
		String methodName = args[0];
		int iterationLimit = Integer.parseInt(args[1]);
		String fileName = args[2];
		
		if (methodName.equals("grad")) {
			ReadingWrapper readingWrapper = Sustav.readFromFile(fileName);
			IFunction f = new TransferFunction(readingWrapper.getX(), readingWrapper.getY());
			RealVector solution = NumOptAlgorithms.calculateGradientDescent(
					f,
					iterationLimit);
			System.out.println("solution = " + solution);
			System.out.println("f(solution) = " + f.getFunctionValueAt(solution));
			System.out.println("df(solution) = " + f.getGradientValueAt(solution));
		} else if (methodName.equals("newton")) {
			ReadingWrapper readingWrapper = Sustav.readFromFile(fileName);
			IHFunction f = new TransferFunction(readingWrapper.getX(), readingWrapper.getY());
			RealVector solution = NumOptAlgorithms.calculateNewtonsMethod(
					f,
					iterationLimit);
			System.out.println("solution = " + solution);
			System.out.println("f(solution) = " + f.getFunctionValueAt(solution));
			System.out.println("df(solution) = " + f.getGradientValueAt(solution));
		} else {
			System.out.println("Unknown method provided: " + methodName);
			System.out.println("Expected: 'grad' or 'newton'");
		}
	}

}
