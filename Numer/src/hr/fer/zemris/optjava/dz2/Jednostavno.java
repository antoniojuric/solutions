package hr.fer.zemris.optjava.dz2;

import hr.fer.zemris.numer.F1;
import hr.fer.zemris.numer.F2;
import hr.fer.zemris.numer.NumOptAlgorithms;

public class Jednostavno {

	public static void main(String[] args) {
		if (args.length != 2 && args.length != 4) {
			System.out.println("Argumenti: zadatak broj_iteracija (x1, y1)");
			return;
		}
		
		String task = args[0];
		int iterationLimit = Integer.parseInt(args[1]);
		Double x = null, y = null;
		if (args.length == 4) {
			x = Double.parseDouble(args[2]);
			y = Double.parseDouble(args[3]);
		}
		
		if (task.equals("1a")) {
			F1 f1 = new F1();
			System.out.println(NumOptAlgorithms.calculateGradientDescent(f1, iterationLimit, x, y));
		} else if (task.equals("1b")) {
			F1 f1 = new F1();
			System.out.println(NumOptAlgorithms.calculateNewtonsMethod(f1, iterationLimit, x, y));
		} else if (task.equals("2a")) {
			F2 f2 = new F2();
			System.out.println(NumOptAlgorithms.calculateGradientDescent(f2, iterationLimit, x, y));
		} else if (task.equals("2b")) {
			F2 f2 = new F2();
			System.out.println(NumOptAlgorithms.calculateNewtonsMethod(f2, iterationLimit, x, y));
		}
	}

}
