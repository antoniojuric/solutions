package hr.fer.zemris.numer;

import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.BlockRealMatrix;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;

/**
 * f2(x1, x2) = (x1 - 1)^2 + 10*(x2 - 2)^2
 * @author ajuric
 *
 */
public class F2 implements IHFunction {

	@Override
	public int getNumberOfVariables() {
		return 2;
	}

	/**
	 * f2(x1, x2) = (x1 - 1)^2 + 10*(x2 - 2)^2
	 */
	@Override
	public double getFunctionValueAt(RealVector point) {
		return Math.pow(point.getEntry(0), 2) + 10*Math.pow(point.getEntry(1) - 2, 2);
	}

	/**
	 * df2(x1, x2) = [2*x1 - 2, 20*x2 - 40]
	 */
	@Override
	public RealVector getGradientValueAt(RealVector point) {
		return new ArrayRealVector(new double[]{
				2 * point.getEntry(0) - 2, 20 * point.getEntry(1) - 40});
	}

	@Override
	public RealMatrix getHesseMatrix(RealVector point) {
		return new BlockRealMatrix(new double[][]{{2, 0}, {0, 20}});
	}
	
	@Override
	public String toString() {
		return "f2(x1, x2) = (x1 - 1)^2 + 10*(x2 - 2)^2";
	}

}
