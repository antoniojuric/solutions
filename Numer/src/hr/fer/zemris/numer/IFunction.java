package hr.fer.zemris.numer;

import org.apache.commons.math3.linear.RealVector;

public interface IFunction {
	
	public int getNumberOfVariables();
	public double getFunctionValueAt(RealVector point);
	public RealVector getGradientValueAt(RealVector point);

}
