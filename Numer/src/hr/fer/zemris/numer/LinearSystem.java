package hr.fer.zemris.numer;

import java.util.List;

import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.BlockRealMatrix;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;

public class LinearSystem implements IHFunction {
	
	private List<RealVector> x;
	private List<Double> y;
	
	private RealMatrix hesseMatrix;

	public LinearSystem(List<RealVector> x, List<Double> y) {
		this.x = x;
		this.y = y;
		hesseMatrix = null; // for reset
	}

	@Override
	public int getNumberOfVariables() {
		return x.get(0).getDimension();
	}

	@Override
	public double getFunctionValueAt(RealVector point) {
		double value = 0;
		for (int i = 0; i < x.size(); i++) {
			value += Math.pow(x.get(i).dotProduct(point) - y.get(i), 2);
		}
		return value;
	}

	@Override
	public RealVector getGradientValueAt(RealVector point) {
		ArrayRealVector gradient = new ArrayRealVector(getNumberOfVariables());
		for (int j = 0; j < getNumberOfVariables(); j++) {
			double ithValue = 0;
			for (int i = 0; i < x.size(); i++) {
				ithValue += x.get(i).getEntry(j) * (x.get(i).dotProduct(point) - y.get(i));
			}
			ithValue *= 2;
			gradient.setEntry(j, ithValue);
		}
		return gradient;
	}

	@Override
	public RealMatrix getHesseMatrix(RealVector point) {
		if (hesseMatrix == null) {
			hesseMatrix = new BlockRealMatrix(getNumberOfVariables(), getNumberOfVariables());
			for (int j = getNumberOfVariables() - 1; j >= 0; j--) {
				for (int k = getNumberOfVariables() - 1; k >= 0; k--) {
					double value = 0;
					for (int i = x.size() - 1; i >= 0; i--) {
						value += x.get(i).getEntry(j) * x.get(i).getEntry(k);
					}
					value *= 2;
					hesseMatrix.setEntry(j, k, value);
				}
			}
		}
		return hesseMatrix;
	}
	
	@Override
	public String toString() {
		return "linear system";
	}

}
