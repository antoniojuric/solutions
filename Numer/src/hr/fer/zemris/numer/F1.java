package hr.fer.zemris.numer;

import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.BlockRealMatrix;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;

/**
 * f1(x1, x2) = x1^2 + (x2 - 1)^2
 * @author ajuric
 *
 */
public class F1 implements IHFunction {

	@Override
	public int getNumberOfVariables() {
		return 2;
	}

	/**
	 * f1(x1, x2) = x1^2 + (x2 - 1)^2
	 */
	@Override
	public double getFunctionValueAt(RealVector point) {
		return Math.pow(point.getEntry(0), 2) + Math.pow(point.getEntry(1) - 1, 2);
	}

	/**
	 * df1(x1, x2) = [2*x1, 2*x2 - 2]
	 */
	@Override
	public RealVector getGradientValueAt(RealVector point) {
		return new ArrayRealVector(new double[]{2* point.getEntry(0), 2*point.getEntry(1) - 2});
	}
	
	@Override
	public RealMatrix getHesseMatrix(RealVector point) {
		return new BlockRealMatrix(new double[][]{{2, 0}, {0, 2}});
	}

	
	@Override
	public String toString() {
		return "f1(x1, x2) = x1^2 + (x2 - 1)^2";
	}

}
