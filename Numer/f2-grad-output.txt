Gradient descent for f2(x1, x2) = (x1 - 1)^2 + 10*(x2 - 2)^2, iteration limit:100
Starting point: {5; 5}
Iteration number: 0,	 currentValue: {5; 5}
Iteration number: 1,	 currentValue: {4.5936113577; 1.9520851827}
Iteration number: 2,	 currentValue: {1.4881887252; 2.366141527}
Iteration number: 3,	 currentValue: {1.438590137; 1.9941521327}
Iteration number: 4,	 currentValue: {1.0595822988; 2.0446865005}
Iteration number: 5,	 currentValue: {1.053528906; 1.9992862815}
Iteration number: 6,	 currentValue: {1.0072732401; 2.0054537013}
Iteration number: 7,	 currentValue: {1.0065343244; 1.9999130816}
Iteration number: 8,	 currentValue: {1.0008886681; 2.0006640563}
Iteration number: 9,	 currentValue: {1.000798024; 1.9999867189}
Iteration number: 10,	 currentValue: {1.0001851416; 2.0000887179}
Iteration number: 11,	 currentValue: {1.000167368; 2.0000035487}
Iteration number: 12,	 currentValue: {1.0000388294; 1.9999762946}
Iteration number: 13,	 currentValue: {1.0000351017; 1.9999990518}
Iteration number: 14,	 currentValue: {1.0000081436; 2.0000063341}
Iteration number: 15,	 currentValue: {1.0000073618; 2.0000002534}
Iteration number: 16,	 currentValue: {1.0000017079; 1.9999983075}
Iteration number: 17,	 currentValue: {1.000001544; 1.9999999323}
Iteration number: 18,	 currentValue: {1.0000009511; 2.0000001923}
Iteration number: 19,	 currentValue: {1.0000008598; 2.0000000077}
Iteration number: 20,	 currentValue: {1.0000001995; 1.9999999486}
Iteration number: 21,	 currentValue: {1.0000001803; 1.9999999979}
{1.0000001803; 1.9999999979}
