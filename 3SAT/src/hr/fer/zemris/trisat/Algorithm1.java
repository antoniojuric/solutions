package hr.fer.zemris.trisat;

public class Algorithm1 extends Algorithm {

	@Override
	public BitVector isSatisfiable(SATFormula formula) {
		BitVector foundSolution = null;
		int numberOfVariables = formula.getNumberOfVariables();
		int possibleValues = (1 << numberOfVariables);
		for (int i = 0; i < possibleValues; i++) {
			BitVector currentBitVector = SATService.convertToBitVector(i, numberOfVariables);
			if (formula.isSatisfied(currentBitVector)) {
				foundSolution = currentBitVector;
				System.out.println(currentBitVector);
			}
		}
		return foundSolution;
	}

}
