package hr.fer.zemris.trisat;

import java.io.IOException;

import hr.fer.zemris.trisat.AlgorithmService.Algo;

public class TriSatSolver {
	
	public static void main(String[] args) {
		
		if (args.length != 2) {
			System.out.println("Program expects two arguments: algorithm number and input filename.");
			return;
		}
		
		Algo selectedAlgorithm = SATService.giveAlgoEnum(args[0]);
		String filename = args[1];
		
		SATFormula formula = null;
		try {
			formula = SATService.parseFile(filename);
		} catch (IOException e) {
			System.out.println("Error while reading file.");
		}
		
		Algorithm algorithm = AlgorithmService.provideAlgorithm(selectedAlgorithm);
		BitVector solution = algorithm.isSatisfiable(formula);
		if (solution != null) {
			System.out.println("Found solution: " + solution);
		} else {
			System.out.println("Solution not found or it's really unsatisfaible.");
		}
	}

}
