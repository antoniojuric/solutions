package hr.fer.zemris.trisat;

import java.util.Iterator;

public class BitVectorNGenerator implements Iterable<MutableBitVector> {
	
	private BitVector assignment;

	public BitVectorNGenerator(BitVector assignment) {
		this.assignment = assignment;
	}

	@Override
	public Iterator<MutableBitVector> iterator() {
		return new MutableBitVectorIterator(assignment.copy());
	}
	
	public MutableBitVector[] createNeighborhood() {
		MutableBitVector[] neighborhood = new MutableBitVector[assignment.getSize()];
		int i = 0;
		for (MutableBitVector neighbor : new BitVectorNGenerator(assignment)) {
			neighborhood[i++] = neighbor.copy();
		}
		return neighborhood;
	}
	
	private class MutableBitVectorIterator implements Iterator<MutableBitVector> {
		
		private MutableBitVector assignment;
		private int neighbours;
		private int currentNeighbour;

		public MutableBitVectorIterator(MutableBitVector assignment) {
			this.assignment = assignment;
			this.neighbours = assignment.getSize();
			this.currentNeighbour = 0;
		}
		
		@Override
		public boolean hasNext() {
			return currentNeighbour < neighbours;
		}

		@Override
		public MutableBitVector next() {
			// TODO mozda promijeniti da uvijek vraca novi???
			if (currentNeighbour > 0) {
				// revert previous
				assignment.set(currentNeighbour - 1, assignment.get(currentNeighbour - 1) ^ true); 
			}
			// revert current
			assignment.set(currentNeighbour, assignment.get(currentNeighbour) ^ true);
			currentNeighbour++;
			return assignment;
		}
		
	}

}
