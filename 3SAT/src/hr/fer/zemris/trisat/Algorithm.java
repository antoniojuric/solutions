package hr.fer.zemris.trisat;

public abstract class Algorithm {

	public abstract BitVector isSatisfiable(SATFormula formula);
}
