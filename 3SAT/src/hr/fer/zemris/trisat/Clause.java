package hr.fer.zemris.trisat;

public class Clause {
	
	private int[] indexes;
	private boolean[] complements;

	public Clause(int[] indexes, boolean[] complements) {
		if (indexes.length != complements.length) {
			throw new IllegalArgumentException("Lengths of indexes and complements must be the same.");
		}
		this.indexes = new int[indexes.length];
		this.complements = new boolean[indexes.length];
		for (int i = indexes.length - 1; i >= 0; i--) {
			this.indexes[i] = indexes[i];
			this.complements[i] = complements[i];
		}
	}
	
	// vraća broj literala koji čine klauzulu
	public int getSize() {
		return indexes.length;
	}
	
	//vraća indeks varijable koja je index-ti član ove klauzule
	public int getLiteral(int index) {
		if (index < 0 || index >= indexes.length) {
			throw new IndexOutOfBoundsException("Index must be in [0, length> range.");
		}
		return indexes[index];
	}
	
	//vraća true ako predana dodjela zadovoljava ovu klauzulu
	public boolean isSatisfied(BitVector assignment) {
		for (int i = 0; i < indexes.length; i++) {
			if (assignment.get(indexes[i]) ^ complements[i]) {
				return true;
			}
		}
		return false;
	}
	
	@Override
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append('(');
		for (int i = 0; i < indexes.length; i++) {
			if (complements[i]) {
				stringBuilder.append('-');
			}
			stringBuilder.append(Integer.toString(indexes[i] + 1));
			if (i < indexes.length - 1) {
				stringBuilder.append(' ');
			}
		}
		stringBuilder.append(')');
		return stringBuilder.toString();
	}
}
