package hr.fer.zemris.trisat;

public class SATFormula {
	
	private int numberOfVariables;
	private Clause[] clauses;

	public SATFormula(int numberOfVariables, Clause[] clauses) {
		this.numberOfVariables = numberOfVariables;
		this.clauses = clauses;
	}
	
	public int getNumberOfVariables() {
		return numberOfVariables;
	}
	
	public int getNumberOfClauses() {
		return clauses.length;
	}
	
	public Clause getClause(int index) {
		if (index < 0 || index >= clauses.length) {
			throw new IndexOutOfBoundsException("Index must be in [0, length> range.");
		}
		return clauses[index];
	}
	
	public boolean isSatisfied(BitVector assignment) {
		return countSatisfiedClauses(assignment) == getNumberOfClauses();
	}
	
	public int countSatisfiedClauses(BitVector assignment) {
		int satisfied = 0;
		for (Clause clause : clauses) {
			if (clause.isSatisfied(assignment)) {
				satisfied++;
			}
		}
		return satisfied;
	}
	
	@Override
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder();
		for (Clause clause : clauses) {
			stringBuilder.append(clause.toString());
		}
		return stringBuilder.toString();
	}
}
