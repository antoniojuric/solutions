package hr.fer.zemris.trisat;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class Algorithm2 extends Algorithm {
	
	private static final int ITERATIONS = 100_000;
	
	private SATFormula formula;
	private Random random = new Random();

	@Override
	public BitVector isSatisfiable(SATFormula formula) {
		this.formula = formula;
		
		// first, create random solution
		BitVector currentSolution = new BitVector(formula.getNumberOfVariables());
		if (formula.isSatisfied(currentSolution)) { // check for the solution at the beginning
			return currentSolution;
		}
		
		int fitX = fitnessFunction(currentSolution);
		for (int i = 0; i < ITERATIONS; i++) {
			MutableBitVector[] neighborhood = 
					new BitVectorNGenerator(currentSolution).createNeighborhood();
			int newMaxFitX = calcMaxFitness(neighborhood);
			if (newMaxFitX < fitX) {
				return null; // failure
			}
			List<MutableBitVector> bestNeighbors = filterByFitness(neighborhood, newMaxFitX);
			currentSolution = pickRandom(bestNeighbors);
			fitX = fitnessFunction(currentSolution);
			if (formula.isSatisfied(currentSolution)) { // solution found
				return currentSolution;
			}
		}
		return null; // solution not found, iterations limit reached
	}
	
	private BitVector pickRandom(List<MutableBitVector> bestNeighbors) {
		return bestNeighbors.get(random.nextInt(bestNeighbors.size()));
	}

	private List<MutableBitVector> filterByFitness(
			MutableBitVector[] neighborhood, int filterFitX) {
		List<MutableBitVector> list = new LinkedList<>();
		for (MutableBitVector neighbor : neighborhood) {
			if (formula.countSatisfiedClauses(neighbor) == filterFitX) {
				list.add(neighbor);
			}
		}
		return list;
	}

	private int calcMaxFitness(MutableBitVector[] neighborhood) {
		int currentMax = 0;
		for (MutableBitVector neighbor : neighborhood) {
			int currentFitX = formula.countSatisfiedClauses(neighbor);
			currentMax = currentFitX > currentMax ? currentFitX : currentMax;
		}
		return currentMax;
	}

	private int fitnessFunction(BitVector assignment) {
		return formula.countSatisfiedClauses(assignment);
	}

}
