package hr.fer.zemris.trisat;

public class SATFormulaStats {
	
	public static final int numberOfBest = 2;
	private static final double percentageConstantUp = 0.01;
	private static final double percentageConstantDown = 0.1;
	private static final double percentageUnitAmount = 50;
	
	
	private SATFormula formula;
	
	private int numberOfSatisfied;
	private boolean isSatisfied;
	private double[] post;
	private boolean[] clausesSatisfied;
	
	public SATFormulaStats(SATFormula formula) {
		this.formula = formula;
		this.post = new double[formula.getNumberOfClauses()];
		this.clausesSatisfied = new boolean[formula.getNumberOfClauses()];
	}
	
	//analizira se predano rješenje i pamte svi relevantni pokazatelji
	public void setAssignment(BitVector assignment, boolean updatePercentages) {
		numberOfSatisfied = formula.countSatisfiedClauses(assignment);
		isSatisfied = formula.getNumberOfClauses() == numberOfSatisfied;
		for (int i = 0, numberOfClauses = formula.getNumberOfClauses(); i < numberOfClauses; i++) {
			if (formula.getClause(i).isSatisfied(assignment)) {
				if (updatePercentages) {
					post[i] += (1 - post[i]) * percentageConstantUp;
				}
				clausesSatisfied[i] = true;
			} else {
				if (updatePercentages) {
					post[i] += (0 - post[i]) * percentageConstantDown;
				}
				clausesSatisfied[i] = false;
			}
		}
	}
	
	//vraća temeljem onoga što je setAssignment zapamtio: broj klauzula koje su zadovoljene
	public int getNumberOfSatisfied() {
		return numberOfSatisfied;
	}
	
	//vraća temeljem onoga što je setAssignment zapamtio
	public boolean isSatisfied() {
		return isSatisfied;
	}
	
	//vraća temeljem onoga što je setAssignment zapamtio: suma korekcija klauzula
	public double getPercentageBonus() {
		double percentageBonus = 0;
		for (int i = 0, numberOfClauses = formula.getNumberOfClauses(); i < numberOfClauses; i++) {
			if (clausesSatisfied[i]) {
				percentageBonus += percentageUnitAmount * (1 - post[i]);
			} else {
				percentageBonus -= percentageUnitAmount * (1 - post[i]);
			}
		}
		return percentageBonus;
	}
	
	//vraća temeljem onoga što je setAssignment zapamtio: procjena postotka za klauzulu
	public double getPercentage(int index) {
		if (index < 0 || index >= post.length) {
			throw new IndexOutOfBoundsException("Index must be in [0, length> range.");
		}
		return post[index];
	}

}
