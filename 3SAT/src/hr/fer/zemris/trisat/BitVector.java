package hr.fer.zemris.trisat;

import java.util.Random;

public class BitVector {
	
	protected boolean[] bits;
	private Random random;
	
	public BitVector(Random rand, int numberOfBits) {
		this.random = rand;
		bits = new boolean[numberOfBits];
		for (int i = 0; i < numberOfBits; i++) {
			bits[i] = random.nextBoolean();
		}
	}
	
	public BitVector(boolean ... bits) {
		this.bits = new boolean[bits.length];
		for (int i = bits.length - 1; i >= 0; i--) { 
			this.bits[i] = bits[i];
		}
	}
	
	public BitVector(int n) {
		this(new Random(), n);
	}
	
	// vraća vrijednost index-te varijable
	public boolean get(int index) {
		if (index < 0 || index >= bits.length) {
			throw new IndexOutOfBoundsException("Index must be in [0, length> range.");
		}
		return bits[index];
	}
	
	// vraća broj varijabli koje predstavlja
	public int getSize() {
		return bits.length;
	}
	
	@Override
	public String toString() {
		StringBuilder stringBuilder = new StringBuilder(bits.length);
		for (boolean bit : bits) {
			stringBuilder.append(bit ? '1' : '0');
		}
		return stringBuilder.toString();
	}
	
	// vraća promjenjivu kopiju trenutnog rješenja
	public MutableBitVector copy() {
		return new MutableBitVector(bits);
	}
}
