package hr.fer.zemris.trisat;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class Algorithm3 extends Algorithm {
	
	private static final int ITERATIONS = 100_000;
	
	private Random random = new Random();

	@Override
	public BitVector isSatisfiable(SATFormula formula) {		
		SATFormulaStats formulaStats = new SATFormulaStats(formula);
		
		// first, create random solution
		BitVector currentSolution = new BitVector(formula.getNumberOfVariables());
		formulaStats.setAssignment(currentSolution, true);
		if (formulaStats.isSatisfied()) { // check for the solution at the beginning
			return currentSolution;
		}
		
		for (int i = 0; i < ITERATIONS; i++) {
			List<NeighborWrapper> neighbors = new ArrayList<>(currentSolution.getSize());
			for (MutableBitVector neighbor : new BitVectorNGenerator(currentSolution)) {
				formulaStats.setAssignment(neighbor, false);
				if (formulaStats.isSatisfied()) {
					return neighbor;
				}
				double Z = formulaStats.getNumberOfSatisfied();
				Z += formulaStats.getPercentageBonus();
				neighbors.add(new NeighborWrapper(Z, neighbor));
			}
			
			List<NeighborWrapper> bestNeighbors = neighbors.stream()
					.sorted((n1, n2) -> - (int) (n1.getZ() - n2.getZ())).limit(SATFormulaStats.numberOfBest)
					.collect(Collectors.toList());
			
			currentSolution = pickRandom(bestNeighbors);
			formulaStats.setAssignment(currentSolution, true);
			if (formulaStats.isSatisfied()) { // solution found
				return currentSolution;
			}
		}
		
		return null; // solution not found, iterations limit reached
	}
	
	private BitVector pickRandom(List<NeighborWrapper> bestNeighbors) {
		return bestNeighbors.get(random.nextInt(bestNeighbors.size())).getNeighbor();
	}
	
	private static class NeighborWrapper {
		
		private double Z;
		private MutableBitVector neighbor;
		
		public NeighborWrapper(double z, MutableBitVector neighbor) {
			Z = z;
			this.neighbor = neighbor;
		}

		public double getZ() {
			return Z;
		}

		public MutableBitVector getNeighbor() {
			return neighbor;
		}
		
		@Override
		public String toString() {
			return "Z=" + Z + ", " + neighbor;
		}
		
	}

}
