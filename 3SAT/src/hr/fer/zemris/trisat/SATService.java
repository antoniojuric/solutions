package hr.fer.zemris.trisat;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;

import hr.fer.zemris.trisat.AlgorithmService.Algo;

public class SATService {
	
	public static SATFormula parseFile(String filename) throws IOException {
		List<String> lines = Files.readAllLines(Paths.get(filename), StandardCharsets.UTF_8);
		
		// filtering lines
		List<String> filteredLines = new LinkedList<>();
		for (String line : lines) {
			line = line.trim();
			if (line.startsWith("c")) { // comment line
				continue;
			}
			if (line.startsWith("%")) { // end of data
				break;
			}
			filteredLines.add(line);
		}
		
		// parsing data lines
		String[] args = filteredLines.get(0).split("[\\s]+");
		int numberOfVariables = Integer.parseInt(args[2]);
		int numberOfClaueses = Integer.parseInt(args[3]);
		filteredLines.remove(0);
		Clause[] clauses = new Clause[numberOfClaueses];
		
		int currentClause = 0;
		for (String line : filteredLines) {
			String[] literals = line.split("[\\s]+");
			
			boolean[] complements = new boolean[literals.length - 1];
			int[] indexes = new int[literals.length - 1];
			
			for (int i = 0; i < literals.length - 1; i++) {
				if (literals[i].startsWith("-")) { // complement
					complements[i] = true;
					indexes[i] = Integer.parseInt(literals[i].substring(1)) - 1;
				} else { // literal
					complements[i] = false;
					indexes[i] = Integer.parseInt(literals[i]) - 1;
				}
			}
			clauses[currentClause++] = new Clause(indexes, complements);
		}
		
		return new SATFormula(numberOfVariables, clauses);
	}
	
	public static BitVector convertToBitVector(int number, int numberOfVariables) {
		MutableBitVector mutableBitVector = new MutableBitVector(numberOfVariables);
		for (int i = 0; i < numberOfVariables; i++) {
			mutableBitVector.set(i, ((number >> i) & 1) == 1);
		}
		return mutableBitVector;
	}
	
	public static Algo giveAlgoEnum(String algoNumber)  {
		algoNumber = algoNumber.trim();
		if (algoNumber.equals("1")) {
			return Algo.ALGORITHM_1;
		} else if (algoNumber.equals("2")) {
			return Algo.ALGORITHM_2;
		} else if (algoNumber.equals("3")) {
			return Algo.ALGORITHM_3;
		} else {
			throw new IllegalArgumentException("Given algorithm number doesn't exist.");
		}
	}

}
