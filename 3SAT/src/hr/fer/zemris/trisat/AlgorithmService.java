package hr.fer.zemris.trisat;

import java.util.HashMap;

public class AlgorithmService {
	
	public enum Algo {
		ALGORITHM_1,
		ALGORITHM_2,
		ALGORITHM_3
	}
	
	private static HashMap<Algo, Algorithm> algorithms;
	
	static {
		algorithms = new HashMap<>();
		algorithms.put(Algo.ALGORITHM_1, new Algorithm1());
		algorithms.put(Algo.ALGORITHM_2, new Algorithm2());
		algorithms.put(Algo.ALGORITHM_3, new Algorithm3());
	}
	
	public static Algorithm provideAlgorithm(Algo algo) {
		return algorithms.get(algo);
	}

}
