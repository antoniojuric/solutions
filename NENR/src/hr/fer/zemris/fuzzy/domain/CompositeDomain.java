package hr.fer.zemris.fuzzy.domain;

import java.util.Iterator;

public class CompositeDomain extends Domain {
	
	private SimpleDomain[] components;
	
	public CompositeDomain(SimpleDomain... components) {
		this.components = components;
		
		generateElements();
	}

	// this implementation in which we create all elements at the beginning is selected
	// because we are not going to do creation a lot, but we will do queries a lot
	private void generateElements() {
		int[] counters = new int[components.length];
		int combinedSize = 1;
		for (int i = 0; i < components.length; i++) {
			counters[i] = 0;
			combinedSize *= components[i].getCardinality();
		}
		elements = new DomainElement[combinedSize];
		
		int i = 0;
		while(true) {
			elements[i++] = generateDomainElement(counters);
			if (!updateCounters(counters)) {
				break;
			}
		}
	}

	private boolean updateCounters(int[] counters) {
		for (int i = components.length - 1; i >= 0; i--) {
			if (counters[i] < components[i].getCardinality() - 1) {
				counters[i]++;
				return true;
			} else {
				counters[i] = 0;
			}
		}
		return false;
	}

	private DomainElement generateDomainElement(int[] counters) {
		int[] values = new int[components.length];
		for (int i = 0; i < components.length; i++) {
			values[i] = components[i].elementForIndex(counters[i]).getComponentValue(0);
		}
		return new DomainElement(values);
	}

	@Override
	public int getCardinality() {
		return elements.length;
	}

	@Override
	public IDomain getComponent(int index) {
		return components[index];
	}

	@Override
	public int getNumberOfComponents() {
		return components.length;
	}

	@Override
	public Iterator<DomainElement> iterator() {
		return new Iterator<DomainElement>() {
			
			private int currentIndex;

			@Override
			public boolean hasNext() {
				return currentIndex < CompositeDomain.this.elements.length;
			}

			@Override
			public DomainElement next() {
				return CompositeDomain.this.elementForIndex(currentIndex++);
			}
		};
	}

}
