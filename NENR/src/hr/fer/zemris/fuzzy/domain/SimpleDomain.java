package hr.fer.zemris.fuzzy.domain;

import java.util.Iterator;

public class SimpleDomain extends Domain {
	
	private int first;
	private int last;
	private int size;

	public SimpleDomain(int first, int last) {
		this.first = first;
		this.last = last;
		this.size = last - first;
		
		elements = new DomainElement[size];
		for (int i = 0; i < size; i++) {
			elements[i] = new DomainElement(new int[]{i + first});
		}
	}

	@Override
	public int getCardinality() {
		return size;
	}

	@Override
	public IDomain getComponent(int index) {
		return this;
	}

	@Override
	public int getNumberOfComponents() {
		return 1;
	}

	public int getFirst() {
		return first;
	}

	public void setFirst(int first) {
		this.first = first;
	}

	public int getLast() {
		return last;
	}

	public void setLast(int last) {
		this.last = last;
	}

	@Override
	public Iterator<DomainElement> iterator() {
		return new Iterator<DomainElement>() {
			
			private int currentIndex;

			@Override
			public boolean hasNext() {
				return currentIndex < SimpleDomain.this.size;
			}

			@Override
			public DomainElement next() {
				return SimpleDomain.this.elementForIndex(currentIndex++);
			}
		};
	}

}
