package hr.fer.zemris.fuzzy.domain;

import java.util.Arrays;

public abstract class Domain implements IDomain {
	
	protected DomainElement[] elements;
	
	public Domain() {}

	/**
	 * Creates domain from range [first, last>.
	 * @param first inclusive begin of range
	 * @param last exclusive end of range
	 * @return
	 */
	public static IDomain intRange(int first, int last) {
		return new SimpleDomain(first, last);
	}
	
	public static Domain combine(IDomain d1, IDomain d2) {
		return new CompositeDomain(new SimpleDomain[]{(SimpleDomain) d1, (SimpleDomain) d2});
	}

	@Override
	public int indexOfElement(DomainElement element) {
		for (int i = 0; i < elements.length; i++) {
			if (element.equals(elements[i])) {
				return i;
			}
		}
		return -1;
	}

	@Override
	public DomainElement elementForIndex(int index) {
		return elements[index];
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(elements);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Domain other = (Domain) obj;
		if (!Arrays.equals(elements, other.elements))
			return false;
		return true;
	}
	
	

}
