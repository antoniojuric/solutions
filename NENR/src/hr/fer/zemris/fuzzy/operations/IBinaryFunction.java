package hr.fer.zemris.fuzzy.operations;

public interface IBinaryFunction {
	
	public double valueAt(double valueA, double valueB);

}
