package hr.fer.zemris.fuzzy.operations;

public class StandardFuzzySets {
	
	public static IIntUnaryFunction lFunction(int alpha, int beta) {
		return new IIntUnaryFunction() {
			
			@Override
			public double valueAt(int x) {
				if (x < alpha) {
					return 1;
				} else if (x < beta) {
					return (beta - x) / (double) (beta - alpha);
				} else {
					return 0;
				}
			}
		};
	}
	
	public static IIntUnaryFunction gammaFunction(int alpha, int beta) {
		return new IIntUnaryFunction() {
			
			@Override
			public double valueAt(int x) {
				if (x < alpha) {
					return 0;
				} else if (x < beta) {
					return (x - alpha) / (double) (beta - alpha);
				} else {
					return 1;
				}
			}
		};
	}
	
	public static IIntUnaryFunction lambdaFunction(int alpha, int beta, int gama) {
		return new IIntUnaryFunction() {
			
			@Override
			public double valueAt(int x) {
				if (x < alpha)  {
					return 0;
				} else if (x < beta) {
					return (x - alpha) / (double) (beta - alpha);
				} else if (x < gama) {
					return (gama - x) / (double) (gama - beta);
				} else {
					return 0;
				}
			}
		};
	}

}
