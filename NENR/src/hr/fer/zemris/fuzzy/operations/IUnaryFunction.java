package hr.fer.zemris.fuzzy.operations;

public interface IUnaryFunction {
	
	public double valueAt(double value);

}
