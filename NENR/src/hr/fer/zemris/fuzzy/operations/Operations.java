package hr.fer.zemris.fuzzy.operations;

import hr.fer.zemris.fuzzy.domain.DomainElement;
import hr.fer.zemris.fuzzy.relations.IFuzzySet;
import hr.fer.zemris.fuzzy.relations.MutableFuzzySet;

public class Operations {
	
	private static IUnaryFunction zadehNot;
	private static IBinaryFunction zadehAnd;
	private static IBinaryFunction zadehOr;
	
	public static IFuzzySet unaryOperation(IFuzzySet set, IUnaryFunction operation) {
		MutableFuzzySet resultSet = new MutableFuzzySet(set.getDomain());
		for (DomainElement element : set.getDomain()) {
			resultSet.set(element, operation.valueAt(set.getValueAt(element)));
		}
		return resultSet;
	}
	
	public static IFuzzySet binaryOperation(IFuzzySet set1, IFuzzySet set2, IBinaryFunction operation) {
		if (!set1.getDomain().equals(set2.getDomain())) {
			throw new IllegalArgumentException("Sets must be defined over same domain.");
		}
		MutableFuzzySet resultSet = new MutableFuzzySet(set1.getDomain());
		for (DomainElement element : set1.getDomain()) {
			resultSet.set(element, operation.valueAt(set1.getValueAt(element), set2.getValueAt(element)));
		}
		return resultSet;
	}
	
	public static IUnaryFunction zadehNot() {
		if (zadehNot == null) {
			zadehNot = new IUnaryFunction() {
				
				@Override
				public double valueAt(double value) {
					return 1 - value;
				}
			};
		}
		return zadehNot;
	}
	
	public static IBinaryFunction zadehAnd() {
		if (zadehAnd == null) {
			zadehAnd = new IBinaryFunction() {
				
				@Override
				public double valueAt(double valueA, double valueB) {
					return Math.min(valueA, valueB);
				}
			};
		}
		return zadehAnd;
	}
	
	public static IBinaryFunction zadehOr() {
		if (zadehOr == null) {
			zadehOr = new IBinaryFunction() {
				
				@Override
				public double valueAt(double valueA, double valueB) {
					return Math.max(valueA, valueB);
				}
			};
		}
		return zadehOr;
	}
	
	public static IBinaryFunction hamacherTNorm(double v) {
		return new IBinaryFunction() {
			
			@Override
			public double valueAt(double valueA, double valueB) {
				return (valueA * valueB) / (v + (1 - v)*(valueA + valueB - valueA * valueB));
			}
		};
	}
	
	public static IBinaryFunction hamacherSNorm(double v) {
		return new IBinaryFunction() {
			
			@Override
			public double valueAt(double valueA, double valueB) {
			return (valueA + valueB - (2 - v) * valueA * valueB) / (1 - (1 - v) * valueA * valueB);
			}
		};
	}

}
