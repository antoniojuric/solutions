package hr.fer.zemris.fuzzy.operations;

public interface IIntUnaryFunction {
	
	public double valueAt(int x);

}
