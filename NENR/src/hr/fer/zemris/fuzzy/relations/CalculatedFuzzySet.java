package hr.fer.zemris.fuzzy.relations;

import hr.fer.zemris.fuzzy.domain.DomainElement;
import hr.fer.zemris.fuzzy.domain.IDomain;
import hr.fer.zemris.fuzzy.operations.IIntUnaryFunction;

public class CalculatedFuzzySet implements IFuzzySet {
	
	private IIntUnaryFunction intUnaryFunction;
	private IDomain domain;
	
	public CalculatedFuzzySet(IIntUnaryFunction intUnaryFunction, IDomain domain) {
		this.intUnaryFunction = intUnaryFunction;
		this.domain = domain;
	}

	@Override
	public IDomain getDomain() {
		return domain;
	}

	@Override
	public double getValueAt(DomainElement element) {
		return intUnaryFunction.valueAt(domain.indexOfElement(element));
	}

}
