package hr.fer.zemris.fuzzy.relations;

import hr.fer.zemris.fuzzy.domain.DomainElement;
import hr.fer.zemris.fuzzy.domain.IDomain;

public interface IFuzzySet {

	public IDomain getDomain();
	public double getValueAt(DomainElement element);
}
