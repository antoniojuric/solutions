package hr.fer.zemris.fuzzy.demo;

import hr.fer.zemris.fuzzy.domain.Domain;
import hr.fer.zemris.fuzzy.domain.DomainElement;
import hr.fer.zemris.fuzzy.domain.IDomain;
import hr.fer.zemris.fuzzy.operations.StandardFuzzySets;
import hr.fer.zemris.fuzzy.relations.CalculatedFuzzySet;
import hr.fer.zemris.fuzzy.relations.IFuzzySet;
import hr.fer.zemris.fuzzy.relations.MutableFuzzySet;

public class Primjer1Zad1 {
	
	public static void main(String[] args) {
		IDomain d = Domain.intRange(0, 11); // {0,1,...,10}
		
		IFuzzySet set1 = new MutableFuzzySet(d)
		.set(DomainElement.of(0), 1.0)
		.set(DomainElement.of(1), 0.8)
		.set(DomainElement.of(2), 0.6)
		.set(DomainElement.of(3), 0.4)
		.set(DomainElement.of(4), 0.2);
		
		Debug.print(set1, "Set1:");
		
		IDomain d2 = Domain.intRange(-5, 6); // {-5,-4,...,4,5}
		IFuzzySet set2 = new CalculatedFuzzySet(
			StandardFuzzySets.lambdaFunction(
			d2.indexOfElement(DomainElement.of(-4)),
			d2.indexOfElement(DomainElement.of(0)),
			d2.indexOfElement(DomainElement.of(4))
			),
			d2
		);
		Debug.print(set2, "Set2:");
	}

}
